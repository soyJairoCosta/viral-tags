import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserDataService } from 'src/app/services/user-data/user-data.service';
import { PAGES } from 'src/models/constants/pages.constant';

@Injectable({
  providedIn: 'root'
})
export class EmptyUserDataGuard implements CanActivate {

  constructor(private userData: UserDataService, private router: Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (!this.userData.getUserData() && !this.userData.getRedirection()) {
        this.router.navigate([PAGES.HOME.REDIRECTION]);
        return true;
      } else {
        return true;
      }
  }
}
