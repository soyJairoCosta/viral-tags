import { TestBed } from '@angular/core/testing';

import { EmptyUserDataGuard } from './empty-user-data.guard';

describe('EmptyUserDataGuard', () => {
  let guard: EmptyUserDataGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(EmptyUserDataGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
