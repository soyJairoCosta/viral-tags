import { Component, ViewChild, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TitleService } from './services/title/title.service';
import { Router } from '@angular/router';
import { PAGES } from 'src/models/constants/pages.constant';

declare let html2canvas: any;
declare let window: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @ViewChild('mainCard') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;
  private capturedImage: any;

  public titleString: string;
  public showButtons: boolean;
  public showIphoneError: boolean;
  constructor(private translate: TranslateService, private title: TitleService, private router: Router) {
    translate.setDefaultLang('es');
    translate.use('es');
    this.titleString = '';
    this.title.title$.subscribe(
      text => {
        this.titleString = text;
        if (router.url.includes('result')) {
          this.showIphoneError = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
          this.showButtons = true;
        } else {
          this.showButtons = false;
        }
      }
    );
  }

  public captureCanvas() {
    html2canvas(this.screen.nativeElement,
      {
        logging: true,
        letterRendering: 1,
        allowTaint: false,
        useCORS: true
      }).then(canvas => {
        this.canvas.nativeElement.src = canvas.toDataURL();
        this.downloadLink.nativeElement.href = canvas.toDataURL('image/jpeg');
        this.downloadLink.nativeElement.download = 'viral-tags_com' + new Date().getTime() + '.jpg';
        this.downloadLink.nativeElement.click();
      });
  }

  public goHome() {
    this.router.navigate([PAGES.HOME.REDIRECTION]);
  }

}
