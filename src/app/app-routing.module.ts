import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmptyUserDataGuard } from './guards/empty-user-data/empty-user-data.guard';


const routes: Routes = [
  { path: '', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) },
  { path: 'posh-or-hippie', loadChildren: () => import('./pages/posh-hippie/posh-hippie.module').then(m => m.PoshHippieModule) },
  { path: 'crush', loadChildren: () => import('./pages/crush/crush.module').then(m => m.CrushModule) },
  { path: 'energy', loadChildren: () => import('./pages/energy/energy.module').then(m => m.EnergyModule) },
  { path: 'results', loadChildren: () => import('./pages/results/results.module').then(m => m.ResultsModule) },
  {
    path: 'get-data', loadChildren: () => import('./pages/get-data/get-data.module').then(m => m.GetDataModule),
    canActivate: [EmptyUserDataGuard]
  },
  {
    path: 'result-end-quarentene',
    canActivate: [EmptyUserDataGuard],
    loadChildren: () => import('./pages/results/result-end-quarentene/result-end-quarentene.module')
    .then(m => m.ResultEndQuarenteneModule)
  },
  {
    path: 'result-pokemon',
    loadChildren: () => import('./pages/results/result-pokemon/result-pokemon.module')
    .then(m => m.ResultPokemonModule),
    canActivate: [EmptyUserDataGuard]
  },
  {
    path: 'result-rajoy-quotes',
    canActivate: [EmptyUserDataGuard],
    loadChildren: () => import('./pages/results/result-rajoy-quotes/result-rajoy-quotes.module')
    .then(m => m.ResultRajoyQuotesModule)
  },
  {
    path: 'result-animal-crossing',
    canActivate: [EmptyUserDataGuard],
    loadChildren: () => import('./pages/results/result-animal-crossing/result-animal-crossing.module')
    .then(m => m.ResultAnimalCrossingModule)
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
