import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  private lastResults: any;
  constructor() { }

  public setResults(results: any) {
    this.lastResults = results;
  }

  public getResults(): any{
    return this.lastResults;
 }
}
