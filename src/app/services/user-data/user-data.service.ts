import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  private userData: any;

  private redirection: string;

  constructor() { }

  public setUserData(userData: any) {
    this.userData = userData;
  }

  public getUserData(): any {
    return this.userData;
  }

  public setRedirection(redirection: string) {
    this.redirection = redirection;
  }

  public getRedirection(): string {
    return this.redirection;
  }

  public clearData() {
    this.redirection = '';
    this.userData = '';
  }
}
