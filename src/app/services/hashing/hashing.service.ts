import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5';

@Injectable({
  providedIn: 'root'
})
export class HashingService {

  constructor() { }

  public getHashByString(value: string): string {
    let mD5value = Md5.hashStr(value).toString();
    mD5value = parseInt(mD5value.toString(), 16).toLocaleString('fullwide', { useGrouping: false });
    return mD5value.substring(0, 16);
  }
}
