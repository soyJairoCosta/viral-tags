import { TestBed } from '@angular/core/testing';

import { PoshHippieService } from './posh-hippie.service';

describe('PoshHippieService', () => {
  let service: PoshHippieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PoshHippieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
