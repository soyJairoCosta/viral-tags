import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Md5 } from 'ts-md5/dist/md5';
import { RESULT_TYPES } from 'src/models/constants/result-types.constant';
import { PoshHippieResult } from 'src/models/interfaces/posh-hippie-result.interface';
import { ResultsService } from '../results/results.service';

@Injectable({
  providedIn: 'root'
})
export class PoshHippieService {

  constructor(private router: Router, private resultsService: ResultsService) { }

  public calculateResult(value: string) {
    // Transform string to MD5
    let mD5value = Md5.hashStr(value).toString();

    mD5value = parseInt(mD5value.toString(), 16).toLocaleString('fullwide', { useGrouping: false });

    const posh = Number(mD5value.substring(0, 4)) % 100;

    const modern = Number(mD5value.substring(4, 8)) % 100;

    const choni = (Number(mD5value.substring(8, 12)) % 100) * 0.4 + (100 - posh) * 0.6;

    const hippie = (Number(mD5value.substring(12, 16)) % 100) * 0.15 + (100 - posh) * 0.85;

    const poshHippieResult = {
      poshValue: posh,
      modernValue: modern,
      choniValue: choni,
      hippieValue: hippie
    } as PoshHippieResult;

    this.resultsService.setResults(poshHippieResult);
    this.router.navigate(['results', { resultType: RESULT_TYPES.POSH_HIPPIE}]);
  }
}
