import { Component, OnInit } from '@angular/core';
import { PoshHippieService } from 'src/app/services/posh-hippie/posh-hippie.service';

@Component({
  selector: 'app-posh-hippie',
  templateUrl: './posh-hippie.component.html',
  styleUrls: ['./posh-hippie.component.scss']
})
export class PoshHippieComponent implements OnInit {

  constructor(private poshHippieService: PoshHippieService) { }

  ngOnInit(): void {
  }

  public calculateResult(value: string) {
    this.poshHippieService.calculateResult(value);
  }

}
