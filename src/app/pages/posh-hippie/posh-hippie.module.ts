import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PoshHippieRoutingModule } from './posh-hippie-routing.module';
import { PoshHippieComponent } from './posh-hippie.component';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import {  MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [PoshHippieComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    PoshHippieRoutingModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    TranslateModule.forChild()
  ]
})
export class PoshHippieModule { }
