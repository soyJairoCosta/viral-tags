import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PoshHippieComponent } from './posh-hippie.component';

const routes: Routes = [{ path: '', component: PoshHippieComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PoshHippieRoutingModule { }
