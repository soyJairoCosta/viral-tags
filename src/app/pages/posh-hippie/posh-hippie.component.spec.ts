import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoshHippieComponent } from './posh-hippie.component';

describe('PoshHippieComponent', () => {
  let component: PoshHippieComponent;
  let fixture: ComponentFixture<PoshHippieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoshHippieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoshHippieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
