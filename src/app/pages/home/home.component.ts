import { Component, OnInit } from '@angular/core';
import { Tag } from 'src/models/interfaces/tag.interface';
import { TAGS_LIST } from 'src/models/constants/tags-list.constant';
import { TitleService } from 'src/app/services/title/title.service';
import { PAGES } from 'src/models/constants/pages.constant';
import { UserDataService } from 'src/app/services/user-data/user-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public tagList: Array<Tag>;

  constructor(
    private title: TitleService,
    private userData: UserDataService
    ) {
    this.tagList = TAGS_LIST;
    this.title.title$.next(PAGES.HOME.TITLE);
    this.userData.clearData();
   }

  ngOnInit(): void {
  }

}
