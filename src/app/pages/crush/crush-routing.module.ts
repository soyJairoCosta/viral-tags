import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrushComponent } from './crush.component';

const routes: Routes = [{ path: '', component: CrushComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrushRoutingModule { }
