import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrushRoutingModule } from './crush-routing.module';
import { CrushComponent } from './crush.component';


@NgModule({
  declarations: [CrushComponent],
  imports: [
    CommonModule,
    CrushRoutingModule
  ]
})
export class CrushModule { }
