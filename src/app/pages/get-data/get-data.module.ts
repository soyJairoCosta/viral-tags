import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GetDataRoutingModule } from './get-data-routing.module';
import { GetDataComponent } from './get-data.component';
import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [GetDataComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    TranslateModule.forChild(),
    GetDataRoutingModule
  ]
})
export class GetDataModule { }
