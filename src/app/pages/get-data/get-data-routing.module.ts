import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GetDataComponent } from './get-data.component';

const routes: Routes = [{ path: '', component: GetDataComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GetDataRoutingModule { }
