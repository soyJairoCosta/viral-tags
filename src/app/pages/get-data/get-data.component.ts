import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title/title.service';
import { TAGS_LIST } from 'src/models/constants/tags-list.constant';
import { PAGES } from 'src/models/constants/pages.constant';
import { UserDataService } from 'src/app/services/user-data/user-data.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-get-data',
  templateUrl: './get-data.component.html',
  styleUrls: ['./get-data.component.scss']
})
export class GetDataComponent {

  public error: string;
  public errorEmpty: boolean;

  constructor(
    private title: TitleService,
    private userData: UserDataService,
    private router: Router,
    private translate: TranslateService
  ) {
    this.title.title$.next(PAGES.GET_DATA.TITLE);
  }

  public calculateResult(value: string) {
    if (value && value !== '') {
      this.errorEmpty = false;
      this.userData.setUserData(value);
      this.router.navigate([this.userData.getRedirection()]);
    } else {
      this.errorEmpty = true;
    }
  }

}
