import { Component } from '@angular/core';
import { TitleService } from 'src/app/services/title/title.service';
import { UserDataService } from 'src/app/services/user-data/user-data.service';
import { HashingService } from 'src/app/services/hashing/hashing.service';
import { PAGES } from 'src/models/constants/pages.constant';
import { ANIMAL_CROSSING_LIST } from 'src/models/constants/animal-crossing.constant';

@Component({
  selector: 'app-result-animal-crossing',
  templateUrl: './result-animal-crossing.component.html',
  styleUrls: ['./result-animal-crossing.component.scss']
})
export class ResultAnimalCrossingComponent {

  public user: string;
  public villager: any;

  constructor(
    private title: TitleService,
    private userData: UserDataService,
    private hashing: HashingService,
  ) {
    this.title.title$.next(PAGES.RESULTS_ANIMAL_CROSSING.TITLE);
    this.user = this.userData.getUserData();
    const userHashed = this.hashing.getHashByString(this.user);
    this.calculateAnimalCrossing(userHashed);
  }

  private calculateAnimalCrossing(userHashed: string) {
    let firstNumber = Math.floor(Number(userHashed[11]) / 5);
    if (firstNumber > 1) {
      firstNumber -= 1;
    }
    const villager = firstNumber.toString() + userHashed[7] + userHashed[3];
    this.villager = ANIMAL_CROSSING_LIST[Number(villager)];

  }
}
