import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultAnimalCrossingComponent } from './result-animal-crossing.component';

describe('ResultAnimalCrossingComponent', () => {
  let component: ResultAnimalCrossingComponent;
  let fixture: ComponentFixture<ResultAnimalCrossingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultAnimalCrossingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultAnimalCrossingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
