import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResultAnimalCrossingComponent } from './result-animal-crossing.component';

const routes: Routes = [{ path: '', component: ResultAnimalCrossingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultAnimalCrossingRoutingModule { }
