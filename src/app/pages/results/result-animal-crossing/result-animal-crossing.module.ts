import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultAnimalCrossingRoutingModule } from './result-animal-crossing-routing.module';
import { ResultAnimalCrossingComponent } from './result-animal-crossing.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [ResultAnimalCrossingComponent],
  imports: [
    CommonModule,
    ResultAnimalCrossingRoutingModule,
    TranslateModule.forChild()
  ]
})
export class ResultAnimalCrossingModule { }
