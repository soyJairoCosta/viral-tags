import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultRajoyQuotesRoutingModule } from './result-rajoy-quotes-routing.module';
import { ResultRajoyQuotesComponent } from './result-rajoy-quotes.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [ResultRajoyQuotesComponent],
  imports: [
    CommonModule,
    ResultRajoyQuotesRoutingModule,
    TranslateModule.forChild()
  ]
})
export class ResultRajoyQuotesModule { }
