import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title/title.service';
import { UserDataService } from 'src/app/services/user-data/user-data.service';
import { HashingService } from 'src/app/services/hashing/hashing.service';
import { PAGES } from 'src/models/constants/pages.constant';
import { RAJOY_QUOTES } from 'src/models/constants/rajoy-quotes.constant';

@Component({
  selector: 'app-result-rajoy-quotes',
  templateUrl: './result-rajoy-quotes.component.html',
  styleUrls: ['./result-rajoy-quotes.component.scss']
})
export class ResultRajoyQuotesComponent implements OnInit {

  public user: string;
  public quote: string;

  constructor(
    private title: TitleService,
    private userData: UserDataService,
    private hashing: HashingService,
  ) {
    this.title.title$.next(PAGES.RESULTS_RAJOY_QUOTES.TITLE);
    this.user = this.userData.getUserData();
    const userHashed = this.hashing.getHashByString(this.user);

    let choosenNumber = Number(userHashed[14] + userHashed[9]);
    if (choosenNumber > RAJOY_QUOTES.length) {
      choosenNumber = Number(userHashed[12]);
    }
    this.quote = RAJOY_QUOTES[choosenNumber];
    console.log(this.quote);
  }

  ngOnInit(): void {
  }

}
