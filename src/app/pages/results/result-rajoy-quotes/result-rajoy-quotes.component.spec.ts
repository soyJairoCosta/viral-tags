import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultRajoyQuotesComponent } from './result-rajoy-quotes.component';

describe('ResultRajoyQuotesComponent', () => {
  let component: ResultRajoyQuotesComponent;
  let fixture: ComponentFixture<ResultRajoyQuotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultRajoyQuotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultRajoyQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
