import { Component, OnInit, OnDestroy } from '@angular/core';
import { Chart } from 'chart.js';
import { ActivatedRoute } from '@angular/router';
import { RESULT_TYPES } from 'src/models/constants/result-types.constant';
import { PoshHippieResult } from 'src/models/interfaces/posh-hippie-result.interface';
import { TranslateService } from '@ngx-translate/core';
import { POSH_HIPPIE_CHART } from 'src/models/constants/charts-config/posh-hippie-chart.config';
import { ResultsService } from 'src/app/services/results/results.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit, OnDestroy {

  // lineChartData: Chart.ChartDataSets[] = [
  //   {
  //     label: 'My First dataset',
  //     fill: false,
  //     lineTension: 0.1,
  //     backgroundColor: 'rgba(75,192,192,0.4)',
  //     borderColor: 'rgba(75,192,192,1)',
  //     borderCapStyle: 'butt',
  //     borderDash: [],
  //     borderDashOffset: 0.0,
  //     borderJoinStyle: 'miter',
  //     pointBorderColor: 'rgba(75,192,192,1)',
  //     pointBackgroundColor: '#fff',
  //     pointBorderWidth: 1,
  //     pointHoverRadius: 5,
  //     pointHoverBackgroundColor: 'rgba(75,192,192,1)',
  //     pointHoverBorderColor: 'rgba(220,220,220,1)',
  //     pointHoverBorderWidth: 2,
  //     pointRadius: 1,
  //     pointHitRadius: 10,
  //     data: [65, 59, 80, 81, 56, 55, 40],
  //   },
  // ];

  inlinePlugin: any;
  textPlugin: any;



  public colors: any;
  // Chart options and data (styles and so)
  public lineChartData: Chart.ChartDataSets[];
  // Chart type 'line'
  public lineChartType: string;
  // Labels of chart
  public lineChartLabels: Array<string>;
  // No legend required
  public lineChartLegend = false;
  // Responsive mode
  public lineChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          suggestedMax: 100
        }
      }]
    }
  };

  private sub: any;

  constructor(private activatedRouter: ActivatedRoute, private translate: TranslateService, private results: ResultsService) { }

  ngOnInit(): void {
    this.sub = this.activatedRouter.params
      .subscribe((params) => {
        switch (params.resultType) {
          case RESULT_TYPES.POSH_HIPPIE:
            this.poshOrHippie(this.results.getResults());
            break;
          case RESULT_TYPES.CRUSH:
            break;
          case RESULT_TYPES.ENERGY:
            break;
        }
      });
  }

  private poshOrHippie(result: PoshHippieResult) {
    this.lineChartType = 'bar';
    this.lineChartLabels = new Array<string>();
    this.lineChartLabels.push(this.translate.instant('results.posh-hippie.chart-labels.posh'));
    this.lineChartLabels.push(this.translate.instant('results.posh-hippie.chart-labels.choni'));
    this.lineChartLabels.push(this.translate.instant('results.posh-hippie.chart-labels.modern'));
    this.lineChartLabels.push(this.translate.instant('results.posh-hippie.chart-labels.hippie'));

    this.lineChartData = [
      {
        data: [result.poshValue, result.choniValue, result.modernValue, result.hippieValue]
      }
    ];

    this.colors = [
      {
        backgroundColor: POSH_HIPPIE_CHART.backgroundColor,
        borderColor: POSH_HIPPIE_CHART.borderColor,
        borderWidth: POSH_HIPPIE_CHART.borderWidth,
      }
    ];

    this.textPlugin = [{
      id: 'poshHippie',
      beforeDraw(chart: any): any {
        const width = chart.chart.width;
        const height = chart.chart.height;
        const ctx = chart.chart.ctx;
        ctx.restore();
        const fontSize = (height / 114).toFixed(2);
        ctx.font = `${fontSize}em sans-serif`;
        ctx.textBaseline = 'middle';
        const text = '';
        const textX = Math.round((width - ctx.measureText(text).width) / 2);
        const textY = height / 2;
        ctx.fillText(text, textX, textY);
        ctx.save();
      }
    }];

    this.inlinePlugin = this.textPlugin;

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
