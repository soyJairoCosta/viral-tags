import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultEndQuarenteneComponent } from './result-end-quarentene.component';

describe('ResultEndQuarenteneComponent', () => {
  let component: ResultEndQuarenteneComponent;
  let fixture: ComponentFixture<ResultEndQuarenteneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultEndQuarenteneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultEndQuarenteneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
