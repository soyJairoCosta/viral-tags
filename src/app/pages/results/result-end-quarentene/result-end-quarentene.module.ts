import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultEndQuarenteneRoutingModule } from './result-end-quarentene-routing.module';
import { ResultEndQuarenteneComponent } from './result-end-quarentene.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [ResultEndQuarenteneComponent],
  imports: [
    CommonModule,
    ResultEndQuarenteneRoutingModule,
    TranslateModule.forChild()
  ]
})
export class ResultEndQuarenteneModule { }
