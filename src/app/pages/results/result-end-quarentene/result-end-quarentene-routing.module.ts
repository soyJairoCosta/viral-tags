import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResultEndQuarenteneComponent } from './result-end-quarentene.component';

const routes: Routes = [{ path: '', component: ResultEndQuarenteneComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultEndQuarenteneRoutingModule { }
