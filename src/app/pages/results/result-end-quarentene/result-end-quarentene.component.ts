import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title/title.service';
import { PAGES } from 'src/models/constants/pages.constant';
import { HashingService } from 'src/app/services/hashing/hashing.service';
import { UserDataService } from 'src/app/services/user-data/user-data.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-result-end-quarentene',
  templateUrl: './result-end-quarentene.component.html',
  styleUrls: ['./result-end-quarentene.component.scss']
})
export class ResultEndQuarenteneComponent implements OnInit {

  public endOfQuarentene: string;
  public user: string;

  constructor(
    private title: TitleService,
    private hashing: HashingService,
    private userData: UserDataService,
    private translate: TranslateService
  ) {
    this.title.title$.next(PAGES.RESULTS_END_QUARENTENE.TITLE);
    this.user = this.userData.getUserData();
    const userHashed = this.hashing.getHashByString(this.user);
    this.endOfQuarentene = this.calculateNewDate(userHashed);

  }

  ngOnInit(): void {
  }

  private calculateNewDate(userHashed: string): string {
    const currenDate = new Date();

    let year = Math.floor((Number(userHashed[1]) / 2) % 4) + currenDate.getFullYear();
    let month = currenDate.getMonth() + 1 + Number(userHashed[4]);
    if (month > 11) {
      year++;
      month -= 11;
    }
    let day = Number(userHashed[5]) + Number(userHashed[6]) + Number(userHashed[7]) + Number(userHashed[8]);
    if (day > 28) {
      day -= 28;
    }
    const monthString = this.translate.instant(`months.${month}`)
    return this.translate.instant('result-end-quarentene.result', {
      day, month: monthString, year
    });
  }

}
