import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultsRoutingModule } from './results-routing.module';
import { ResultsComponent } from './results.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgChartjsModule } from 'ng-chartjs';


@NgModule({
  declarations: [ResultsComponent],
  imports: [
    CommonModule,
    ResultsRoutingModule,
    TranslateModule.forChild(),
    NgChartjsModule
  ]
})
export class ResultsModule { }
