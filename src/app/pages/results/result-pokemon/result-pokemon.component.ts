import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title/title.service';
import { PAGES } from 'src/models/constants/pages.constant';
import { UserDataService } from 'src/app/services/user-data/user-data.service';
import { HashingService } from 'src/app/services/hashing/hashing.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NO_AVAILABLE_POKEMONS } from 'src/models/constants/no-available-pokemons.constant';


@Component({
  selector: 'app-result-pokemon',
  templateUrl: './result-pokemon.component.html',
  styleUrls: ['./result-pokemon.component.scss']
})
export class ResultPokemonComponent implements OnInit {
  public user: string;
  public pokemonExist: boolean;
  public pokemonNumber: string;
  public pokemonName: string;
  public pokemonBase64: any;
  constructor(
    private title: TitleService,
    private userData: UserDataService,
    private hashing: HashingService,
    private http: HttpClient,
    private domSanitizer: DomSanitizer
  ) {
    this.title.title$.next(PAGES.RESULTS_POKEMON.TITLE);
    this.user = this.userData.getUserData();
    const userHashed = this.hashing.getHashByString(this.user);
    this.calculatePokemon(userHashed);
  }

  ngOnInit(): void {
  }

  private calculatePokemon(userHashed: string) {
    this.pokemonNumber = userHashed[7] + userHashed[3] + userHashed[5];

    if (Number(this.pokemonNumber) > 807) {
      this.pokemonNumber = (Number(this.pokemonNumber) - 807).toString();
    }

    if (NO_AVAILABLE_POKEMONS.includes(this.pokemonNumber)) {
      this.pokemonNumber = (Number(this.pokemonNumber) - 3).toString();
    }

    this.http.get(`https://pokeapi.co/api/v2/pokemon/${Number(this.pokemonNumber)}/`).toPromise().then(
      (response: any) => {
        this.pokemonName = response.name;
        const c: any = document.getElementById('my-canvas');
        const ctx = c.getContext('2d');
        const img2 = new Image();
        img2.crossOrigin = 'Anonymous';
        img2.src = 'https://pokeres.bastionbot.org/images/pokemon/' + this.pokemonNumber + '.png';
        img2.onload = () => {
          c.height = img2.height;
          c.width = img2.width;
          ctx.drawImage(img2, 0, 0);
          this.pokemonBase64 = c.toDataURL();
          this.pokemonExist = true;
        };
      }
    ).catch(error => console.log(error));
  }
}
