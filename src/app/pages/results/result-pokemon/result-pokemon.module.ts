import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultPokemonRoutingModule } from './result-pokemon-routing.module';
import { ResultPokemonComponent } from './result-pokemon.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [ResultPokemonComponent],
  imports: [
    CommonModule,
    ResultPokemonRoutingModule,
    TranslateModule.forChild()
  ]
})
export class ResultPokemonModule { }
