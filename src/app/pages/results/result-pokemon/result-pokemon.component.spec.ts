import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultPokemonComponent } from './result-pokemon.component';

describe('ResultPokemonComponent', () => {
  let component: ResultPokemonComponent;
  let fixture: ComponentFixture<ResultPokemonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultPokemonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultPokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
