import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResultPokemonComponent } from './result-pokemon.component';

const routes: Routes = [{ path: '', component: ResultPokemonComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultPokemonRoutingModule { }
