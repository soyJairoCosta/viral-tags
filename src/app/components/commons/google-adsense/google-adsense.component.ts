import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-google-adsense',
  templateUrl: './google-adsense.component.html',
  styleUrls: ['./google-adsense.component.scss']
})
export class GoogleAdsenseComponent implements AfterViewInit {
  @ViewChild('script') script: ElementRef;
  @ViewChild('container') container: ElementRef;
  constructor() { }


  ngAfterViewInit() {
    // const windowName = 'adsbygoogle';
    // const windowName2 = 'adsbygoogle';

    // const errorName = 'error';

    //setTimeout(() => {
    //   try {
    //     this.convertToScript();
    //   } catch (e) {
    //     console.error(errorName);
    //   }
    // }, 2000);
    this.convertToScript();
  }

  convertToScript() {
    const element = this.script.nativeElement;
    const script = document.createElement('script');
    script.src = 'https://inswebt.com/pw/waWQiOjEwNDA5MTEsInNpZCI6MTA0NTIzMiwid2lkIjo4Mjg1OCwic3JjIjoyfQ==eyJ.js';
    if (element.innerHTML) {
      script.innerHTML = element.innerHTML;
    }
    // script.textContent = `atOptions = {
    //   'key' : '492badba9b4559bcc2c5b88fc8495fef',
    //   'format' : 'iframe',
    //   'height' : 50,
    //   'width' : 320,
    //   'params' : {}
    // };`;
    const parent = element.parentElement;
    parent.parentElement.replaceChild(script, parent);
    this.container.nativeElement
      .append('<script async src="https://inswebt.com/pw/waWQiOjEwNDA5MTEsInNpZCI6MTA0NTIzMiwid2lkIjo4Mjg1OCwic3JjIjoyfQ==eyJ.js"></script>');
  }
}
