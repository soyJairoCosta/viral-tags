import { Component, OnInit, Input } from '@angular/core';
import { Tag } from 'src/models/interfaces/tag.interface';
import { Router } from '@angular/router';
import { PAGES } from 'src/models/constants/pages.constant';
import { UserDataService } from 'src/app/services/user-data/user-data.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() tag: Tag;

  constructor(private router: Router, private userData: UserDataService) { }

  ngOnInit(): void {
  }

  public redirectTo() {
    this.userData.setRedirection(this.tag.redirectTo);
    this.router.navigate([PAGES.GET_DATA.REDIRECTION]);
  }

}
