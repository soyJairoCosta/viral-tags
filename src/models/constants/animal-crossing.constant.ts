export const ANIMAL_CROSSING_LIST = [
    {
      name: 'Agreste',
      description: 'Agreste, al ser de personalidad esnob, será caballeroso, amable y educado con el jugador. Al igual que otros vecinos de su misma personalidad, acariciará su ego de vez en cuando, hablando de lo genial que es. Se llevará bien con el resto de vecinos, aunque pueda entrar en conflicto con algún vecino de personalidad gruñona. Es muy optimista y ocasionalmente coqueteará con una jugadora mujer. Tiene una obsesión con su imagen, y siempre presumirá de la ropa que lleva. En sus conversaciones, hablará de moda entre otros temas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f4/Agreste_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180807014104&path-prefix=es'
    },
    {
      name: 'Aisle',
      description: 'Aisle, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/82/Aisle.png/revision/latest?cb=20190928073442&path-prefix=es'
    },
    {
      name: 'Alba',
      description: 'Alba, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/52/Alba_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191116151153&path-prefix=es'
    },
    {
      name: 'Albino',
      description: 'Albino, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/73/Albino_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180916011928&path-prefix=es'
    },
    {
      name: 'Alderia',
      description: 'Alderia tiene personalidad Normal, por lo que tendrá un carácter amable, cariñoso y tranquilo, Alderia disfruta de los pasatiempos comunes, tales como la pesca y atrapar bichos, generalmente para mantenerse activa. Alderia frecuentemente invitará al Jugador a su casa, donde denotara sus preocupaciones por la higiene y la limpieza. Alderia se sentirá decepcionada u ofendida fácilmente por otros vecinos, en particular con los vecinos perezoso que se despistan fácilmente en sus conversaciones y los vecinos gruñones que tienen una naturaleza grosera e irritable. Ella se llevará bien con otras vecinas normales y vecinas alegres, también se lleve bien con los vecinos esnobs y presumidas, que disfrutan de la buena personalidad afable de Alderia, que a menudo las complementan.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b7/Alderia_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202040706&path-prefix=es'
    },
    {
      name: 'Aliste',
      description: 'Aliste tiene la personalidad gruñona, Aliste por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Aliste invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Aliste también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f2/Aliste_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190316041445&path-prefix=es'
    },
    {
      name: 'Almendra',
      description: 'Almendra, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/93/Almendra_%28New_Horizons%29.png/revision/latest/scale-to-width-down/200?cb=20200202040707&path-prefix=es'
    },
    {
      name: 'Altramuz',
      description: 'Altramuz, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/2f/Altramuz_%28New_Horizons%29.png/revision/latest/scale-to-width-down/200?cb=20200202040708&path-prefix=es'
    },
    {
      name: 'Amalio',
      description: 'Amalio tiene la personalidad esnob, Amalio sera muy educado, amable y caballeroso con el Jugador. Como vecino esnob, Amalio acariciará su ego de vez en cuando, hablando de lo genial que es y ocasionalmente coqueteará con las jugadoras femeninas, como todo vecino esnob, Amalio tiene una obsesión con su imagen y presumirá de su blog personal o la ropa que lleve puesta. En general, Amalio se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con los vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/68/Amalio_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202040710&path-prefix=es'
    },
    {
      name: 'Amanda',
      description: 'Amanda, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/3c/Amanda_%28New_Leaf%29.png/revision/latest/scale-to-width-down/130?cb=20191128143536&path-prefix=es'
    },
    {
      name: 'Amelia',
      description: 'Amelia tiene la personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Amelia primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También hablará sobre el estilo y la apariencia de otros vecinos, por lo general de vecinas normales, alegres y otras vecinas presumidas. Pronto se apegara hacia Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Amelia no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos, porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras y similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/39/Amelia_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202040712&path-prefix=es'
    },
    {
      name: 'Amparo',
      description: 'Amparo tratará al jugador con respeto e incluso se ofrecerá a luchar contra cualquiera que le cause problemas al jugador. Amparo puede llevarse bien con los vecinos atléticos ya que comparten su amor por los deportes, los vecinos perezosos por lo tranquilos que son y las vecinas alegre. Sin embargo, ella puede entrar en conflicto con los vecinos presumida y gruñón, que difunden rumores sobre los demás, y ella puede tener problemas para llevarse bien con las vecinas normales.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/3d/Amparo_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202040713&path-prefix=es'
    },
    {
      name: 'Anabel',
      description: 'Anabel tiene la personalidad alegre, por lo que a menudo parece estar de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Anabel tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Anabel, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos habituales. Anabel sueña con convertirse en famosa en el futuro y leer la Miss Nintendique, una revista invisible leída por vecinas presumida, alegre, normales, y otros vecinos. Anabel también tendrá un lapso de atención muy corto, lo que significa que olvidará algunos argumentos o tareas que se le dieron al jugador que no fueron completadas muy rápido, Anabel puede enojarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezoso, normales, atléticos, dulces, y otros vecinos alegre, pero ella se puede molestar y molestar a los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a su naturaleza de Anabel, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/9c/Anabel_%28Happy_Home_Designer%29.png/revision/latest?cb=20190324004850&path-prefix=es'
    },
    {
      name: 'Anacarda',
      description: 'Anacarda, al ser de personalidad dulce, se comportará de forma cariñosa hacia el jugador y a los vecinos. Se preocupará por la moda, pero no dejará de ser amable y atenta. Cuando se haga amiga del Jugador, le dará consejos para pelear y le enseñará formas de relajarse. Se llevará bien con vecinos de personalidad atlética, perezosa, esnob y alegre, así como también con otras vecinas de su misma personalidad. Podría entrar con conflicto con vecinos de personalidad gruñona o presumida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/96/Anacarda_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191027143834&path-prefix=es'
    },
    {
      name: 'Analogue',
      description: 'Analogue, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/61/Analogue_DnMe%2B.png/revision/latest/scale-to-width-down/100?cb=20190418145655&path-prefix=es'
    },
    {
      name: 'Anquita',
      description: 'Anquita tiene personalidad Normal, por lo que tendrá un carácter amable, cariñoso y tranquilo, Anquita disfruta de los pasatiempos comunes, tales como la pesca y atrapar bichos, generalmente para mantenerse activa. Anquita frecuentemente invitará al Jugador a su casa, donde denotara sus preocupaciones por la higiene y la limpieza. Anquita se sentirá decepcionada u ofendida fácilmente por otros vecinos, en particular con los vecinos perezoso que se despistan fácilmente en sus conversaciones y los vecinos gruñones que tienen una naturaleza grosera e irritable. Ella se llevará bien con otras vecinas normales y vecinas alegres, también se lleve bien con los vecinos esnobs y presumidas, que disfrutan de la buena personalidad afable de Anquita, que a menudo las complementan.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f3/Anquita_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20190811001447&path-prefix=es'
    },
    {
      name: 'Antonio',
      description: 'Antonio tiene la personalidad atlética, Antonio tendrá un interés en la aptitud física y los deportes. Esto lo hará parecer competitivo y desafiante, Antonio tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable y amable con el Jugador. Como todo vecino atlético, Antonio está muy interesado en su hobby, y puede competir contra el jugador por atrapar Bichos o Peces. Antonio se lleva bien con los vecinos esnobs, alegres y dulces, pero tendrá problemas con el estilo de vida relajado de los vecinos perezosos, las vecinos gruñones y presumida también pueden tener problemas para llevarse bien con Antonio.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/46/Antonio_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20190810010534&path-prefix=es'
    },
    {
      name: 'Aníbal',
      description: 'Aníbal, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f1/An%C3%ADbal_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200205013316&path-prefix=es'
    },
    {
      name: 'Apolo',
      description: 'Apolo tiene la personalidad gruñona, Apolo por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Apolo invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Apolo también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/cc/Apolo_%28New_Horizons%29.png/revision/latest/scale-to-width-down/200?cb=20200221070629&path-prefix=es'
    },
    {
      name: 'Aquilino',
      description: 'Aquilino, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/e0/Aquilino_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191020091100&path-prefix=es'
    },
    {
      name: 'Ardelta',
      description: 'Ardelta, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/e6/Ardelta_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180812040142&path-prefix=es'
    },
    {
      name: 'Arduna',
      description: 'Arduna tiene personalidad alegre, parece estar de buen humor a menudo, y es fácil de hacerse amigo de ella. Arduna tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Arduna, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos sus pasatiempos habituales. Las vecinas alegres sueñan con ser famosas en el futuro y leer Miss Nintendique, una revista leída por vecinos presumidas, alegres normales. Arduna también tendrá un lapso de atención muy corto, lo que significa que pronto olvidará algunos argumentos o tareas que se le dieron al jugador y que no fueron completados, Arduna puede enfadarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero ella puede molestarse y molestar a los vecinos gruñones y presumidos, cuyas personalidades difieren. Debido a la naturaleza de las vecinas alegres, ella puede referirse a los vecinos gruñones como viejos o aburridos que se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/4d/Arduna.png/revision/latest?cb=20170329162243&path-prefix=es'
    },
    {
      name: 'Aria',
      description: 'Aria tiene una personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Como una vecina presumida, Aria primero se mostrará grosera y arrogante con el jugador , a menudo hablando de sí misma y de sus propias experiencias. También habla sobre el estilo y la apariencia de otros vecinas, por lo general vecinas normales, alegres y otras vecinas presumidas. Pronto se pondrá en contacto con el jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Aria no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque los vecinos perezosos odian lo que come, y ella no se lleva bien con los vecinos atléticos. porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6f/Aria_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/100?cb=20180612233923&path-prefix=es'
    },
    {
      name: 'Aristo',
      description: 'Aristo tiene la personalidad gruñona, Aristo por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Aristo invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Aristo también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/e6/Aristo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180605031428&path-prefix=es'
    },
    {
      name: 'Arni',
      description: 'Arni, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/88/Arni_NL.png/revision/latest/scale-to-width-down/150?cb=20130710143026&path-prefix=es'
    },
    {
      name: 'Arsenio',
      description: 'Arsenio tiene la personalidad gruñona, Arsenio por lo general parecerá estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Arsenio tiene una edad avanzada e invertirá su tiempo en su hobby, con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con las vecinas alegre, debido a su actitud sobre estimulada e inmadura. Arsenio también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/cf/Arsenio_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20181224053747&path-prefix=es'
    },
    {
      name: 'Artorito',
      description: 'Artorito, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/38/Artorito_%28Happy_Home_Designer%29.png/revision/latest?cb=20180520224057&path-prefix=es'
    },
    {
      name: 'Astaúlfo',
      description: 'Astaúlfo tiene la personalidad gruñona, Astaúlfo por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Astaúlfo invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Astaúlfo también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f6/Asta%C3%BAlfo_%28Poblaci%C3%B3n_en_Aumento%29.png/revision/latest/scale-to-width-down/120?cb=20181129060819&path-prefix=es'
    },
    {
      name: 'Astillas',
      description: 'Astillas, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/5a/Astillas_NL.png/revision/latest/scale-to-width-down/120?cb=20191001195132&path-prefix=es'
    },
    {
      name: 'Astrid',
      description: 'Astrid tiene la personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Como vecina presumida, Astrid primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También habla sobre el estilo y la apariencia de otros vecinos, por lo general vecinas normales, alegres y otras vecinas presumidas. Pronto se pondrá en contacto con el Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Astrid no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos. porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/a0/Astrid_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190620072141&path-prefix=es'
    },
    {
      name: 'Ataúlfo',
      description: 'Ataúlfo, al ser de personalidad esnob, será caballeroso, amable y educado con el jugador. Al igual que otros vecinos de su misma personalidad, acariciará su ego de vez en cuando, hablando de lo genial que es. Se llevará bien con el resto de vecinos, aunque pueda entrar en conflicto con algún vecino de personalidad gruñona. Es muy optimista y ocasionalmente coqueteará con una jugadora mujer. Tiene una obsesión con su imagen, y siempre presumirá de la ropa que lleva. En sus conversaciones, hablará de moda entre otros temas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/d8/Ata%C3%BAlfo_%28New_Horizons%29.png/revision/latest/scale-to-width-down/120?cb=20200202040723&path-prefix=es'
    },
    {
      name: 'Aurelia',
      description: 'Aurelia, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/af/Aurelia_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20181008231430&path-prefix=es'
    },
    {
      name: 'Ava',
      description: 'Ava, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/af/Ava_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20191116092559&path-prefix=es'
    },
    {
      name: 'Avelina',
      description: 'Avelina tiene la personalidad dulce, cuando se haga amiga del Jugador, le dará consejos para pelear y le enseñará formas de relajarse. Avelina también le dará Medicina al jugador si ha sido picado por abejas. Al igual que todos las vecinas dulces, Avelina tendrá un carácter rudo, pero aún se preocupará mucho por su apariencia. Avelina tiende a quedarse despierta hasta muy tarde. Es fácil hacerse una idea de ella, ya que desarrollar amistades con las vecinas dulces es simple debido a su naturaleza afectuosa y protectora. Avelina tratará al jugador con respeto e incluso se ofrecerá a luchar contra cualquiera que le cause problemas. Avelina puede llevarse bien con los vecinos atléticos ya que comparten su amor por los deportes, los vecinos perezosos por lo tranquilos que son y las vecinas alegres. Sin embargo, ella puede entrar en conflicto con las vecinas presumidas y los vecinos gruñones, que difunden rumores sobre los demás, también puede tener problemas para llevarse bien con las vecinas normales.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/9b/Avelina_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202040724&path-prefix=es'
    },
    {
      name: 'Avutardo',
      description: 'Avutardo, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/08/Avutardo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/100?cb=20191117105257&path-prefix=es'
    },
    {
      name: 'Azabache',
      description: 'Azabache parecerá muy grosera y arrogante al principio, pero si se le deja paciencia se calmará un poco. Le gusta hablar de estilo con las vecinas alegres, normales y presumidas. Le molestan los atléticos y perezosos, por sus formas de vestir y de vida. Se lleva bien con los vecinos gruñones. Le gustan las revistas de Miss Nintendique.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/0f/Azabache_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202040726&path-prefix=es'
    },
    {
      name: 'Azucena',
      description: 'Azucena tiene la personalidad alegre, por lo que a menudo estará de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Azucena tendrá la tendencia a reaccionar de forma exagerada en conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Azucena, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos más comunes, sueña con ser famosa en el futuro y disfrutara leer la revista Miss Nintendique. Azucena también tendrá un lapso de atención muy corto, lo que significa que olvidará algunas conversaciones o encargos que le dio al jugador que no fueron completadas muy rápido, Azucena puede enojarse fácilmente en una conversación cuando no se esta de acuerdo con ella. Azucena se llevará bien con los otros vecinos, particularmente con los perezoso, normal, atlético, dulce, y otras vecinas alegres, pero ella se puede molestar y ser molestada por los vecinos gruñón y esnobs, cuyas personalidades difieren. Debido a la naturaleza de Azucena, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/bb/Azucena_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202040727&path-prefix=es'
    },
    {
      name: 'Azulino',
      description: 'Azulino tiene la personalidad esnob, Azulino sera muy educado, amable y caballeroso con el Jugador. Como vecino esnob, Azulino acariciará su ego de vez en cuando, hablando de lo genial que es y ocasionalmente coqueteará con las jugadoras femeninas, como todo vecino esnob, Azulino tiene una obsesión con su imagen y presumirá de su blog personal o la ropa que lleve puesta. En general, Azulino se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con los vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/af/Azulino_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20191121003724&path-prefix=es'
    },
    {
      name: 'Aída',
      description: 'Aída, al ser de personalidad dulce, se comportará de forma cariñosa hacia el jugador y a los vecinos. Se preocupará por la moda, pero no dejará de ser amable y atenta. Cuando se haga amiga del Jugador, le dará consejos para pelear y le enseñará formas de relajarse. Se llevará bien con vecinos de personalidad atlética, perezosa, esnob y alegre, así como también con otras vecinas de su misma personalidad. Podría entrar con conflicto con vecinos de personalidad gruñona o presumida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/e7/A%C3%ADda_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191116145551&path-prefix=es'
    },
    {
      name: 'Babú',
      description: 'Babú tiene la personalidad atlética, y tiene un gran interés en los deportes y el culturismo. Babú es hiperactivo y motivado, pero a menudo se muestra egoísta y denso. Como un vecino atlético, Babú está muy interesado en su hobby, y puede competir contra el jugador por atrapar Bichos o Peces. Babú se lleva bien con los vecinos alegres, gruñones y dulces, pero entran en conflicto con el estilo de vida relajado de los vecinos perezosos. Las vecinas presumidas también pueden ser difíciles de llevarse bien con Babú.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/1/13/Bab%C3%BA_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20190214055746&path-prefix=es'
    },
    {
      name: 'Arándano',
      description: 'Arándano tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Arándano le encanta la comida y el descanso. Arándano disfrutará de participar en los pasatiempos comunes, generalmente por razones que involucran relajarse o comer, como la pesca. Arándano se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y de buen estado físico, en comparación con el estilo de vida perezoso de Arándano. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con vecinos normales, alegres, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/5c/Ar%C3%A1ndano_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20190613063651&path-prefix=es'
    },
    {
      name: 'Beelén',
      description: 'Beelén, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/88/Beel%C3%A9n_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042247&path-prefix=es'
    },
    {
      name: 'Braulio',
      description: 'Braulio, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/66/Braulio_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20190612031312&path-prefix=es'
    },
    {
      name: 'Bow',
      description: 'Bow, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/ae/Bow_DnMe%2B.png/revision/latest/scale-to-width-down/100?cb=20190418123813&path-prefix=es'
    },
    {
      name: 'Boquerón',
      description: 'Boquerón, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/2f/Boquer%C3%B3n_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191108210922&path-prefix=es'
    },
    {
      name: 'Bernabé',
      description: 'Bernabé tiene la personalidad esnob, Bernabé sera muy educado, amable y caballeroso con el Jugador. Como vecino esnob, Bernabé acariciará su ego de vez en cuando, hablando de lo genial que es y ocasionalmente coqueteará con las jugadoras femeninas, como todo vecino esnob, Bernabé tiene una obsesión con su imagen y presumirá de su blog personal o la ropa que lleve puesta. En general, Bernabé se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con los vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/54/Bernab%C3%A9_%28Happy_Home_Designer%29.png/revision/latest?cb=20180706184549&path-prefix=es'
    },
    {
      name: 'Bernardo',
      description: 'Bernardo, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c1/Bernardo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191117121401&path-prefix=es'
    },
    {
      name: 'Belinda',
      description: 'Belinda tiene la personalidad alegre, Belinda parece estar de buen humor a menudo, y es fácil de hacerse amigo de ella. Belinda tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Belinda, al igual que otros vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos sus pasatiempos habituales. Las vecinas alegres sueñan con ser famosas en el futuro y leer Miss Nintendique, una revista leída por vecinos presumidas, alegres normales. Belinda también tendrán un lapso de atención muy corto, lo que significa que pronto olvidará algunos argumentos o tareas que se le dieron al jugador y que no fueron completados, Belinda puede enfadarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero ella puede molestar y molestarse con los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a la naturaleza de las vecinas alegres, ella puede referirse a los vecinos gruñones como viejos o aburridos que se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/1/1c/Belinda_%28New_Horizons%29.png/revision/latest/scale-to-width-down/200?cb=20200221074133&path-prefix=es'
    },
    {
      name: 'Benito',
      description: 'Benito, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/9e/Benito_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191116093526&path-prefix=es'
    },
    {
      name: 'Bella',
      description: 'Bella tiene la personalidad alegre, Bella parece estar de buen humor a menudo, y es fácil de hacerse amigo de ella. Bella tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Bella, al igual que otros vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos sus pasatiempos habituales. Las vecinas alegres sueñan con ser famosas en el futuro y leer Miss Nintendique, una revista leída por vecinos presumidas, alegres normales. Bella también tendrán un lapso de atención muy corto, lo que significa que pronto olvidará algunos argumentos o tareas que se le dieron al jugador y que no fueron completados, Bella puede enfadarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero ella puede molestar y molestarse con los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a la naturaleza de las vecinas alegres, ella puede referirse a los vecinos gruñones como viejos o aburridos que se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/80/Bella_%28Poblaci%C3%B3n_en_aumento%29.png/revision/latest/scale-to-width-down/160?cb=20181006053042&path-prefix=es'
    },
    {
      name: 'Bianca',
      description: 'Bianca tiene la personalidad alegre, por lo que a menudo parece estar de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Bianca tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Bianca, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos habituales. Bianca sueña con convertirse en famosa en el futuro y leer la Miss Nintendique, una revista invisible leída por vecinas presumida, alegre, normales, y otros vecinos. Bianca también tendrá un lapso de atención muy corto, lo que significa que olvidará algunos argumentos o tareas que se le dieron al jugador que no fueron completadas muy rápido, Bianca puede enojarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezoso, normales, atléticos, dulces, y otros vecinos alegre, pero ella se puede molestar y molestar a los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a su naturaleza de Bianca, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/75/Bianca_%28New_Horizons%29.png/revision/latest/scale-to-width-down/120?cb=20200202042250&path-prefix=es'
    },
    {
      name: 'Boni',
      description: 'Boni tiene personalidad Normal, por lo que tendrá un carácter amable, cariñoso y tranquilo, Boni disfruta de los pasatiempos comunes, tales como la pesca y atrapar bichos, generalmente para mantenerse activa. Boni frecuentemente invitará al Jugador a su casa, donde denotara sus preocupaciones por la higiene y la limpieza. Boni se sentirá decepcionada u ofendida fácilmente por otros vecinos, en particular con los vecinos perezoso que se despistan fácilmente en sus conversaciones y los vecinos gruñones que tienen una naturaleza grosera e irritable. Ella se llevará bien con otras vecinas normales y vecinas alegres, también se lleve bien con los vecinos esnobs y presumidas, que disfrutan de la buena personalidad afable de Boni, que a menudo las complementan.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6d/Boni_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180525233248&path-prefix=es'
    },
    {
      name: 'Berta',
      description: 'Berta es agradable, debido a su comportamiento. Le gusta el orden en su casa y su ropa. A veces es molesta, pero también amable, además, es muy higiénica. Le gusta comprar ropa y también puede regalarte algunas prendas que no necesite.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/1/1e/Berta_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191116131101&path-prefix=es'
    },
    {
      name: 'Boliche',
      description: 'Boliche tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Boliche le encanta la comida y el descanso. Boliche disfrutará participar en los pasatiempos comunes, generalmente por razones relajantes o para comer, como la pesca. Boliche se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y buen estado físico, en comparación con el estilo de vida perezoso de Boliche. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con los vecinos normales, alegre, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumida, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/d6/Boliche_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20181224054458&path-prefix=es'
    },
    {
      name: 'Bombo',
      description: 'Bombo tiene la personalidad atlética, por lo que tendrá un gran interés en la aptitud física y los deportes. Esto lo hará parecer muy competitivo y desafiante, Bombo tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, pero a pesar de esto, sera amigable y amable con el Jugador. Como todo vecino atlético, Bombo está muy interesado en su hobby, y puede competir contra el jugador por atrapar Bichos o Peces, Bombo se lleva bien con los vecinos esnobs, alegres y dulces, pero tendrá problemas con el estilo de vida relajado de los vecinos perezosos, las vecinos gruñones y presumida también pueden tener problemas para llevarse bien con Bombo.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/2f/Bombo_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200221074139&path-prefix=es'
    },
    {
      name: 'Bollito',
      description: 'Bollito es un vecino perezoso, lo que significa que va a mostrarse relajado, agradable y fácil de llevar una persona a tratar. Su obsesión por los alimentos le impulsa y le obliga a no entender a los vecinos deportistas, pero su personalidad suele ser tan agradable que todo lo acepta y se hace amigo de casi todos los vecinos. Su muletilla es rebollos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/42/Bollito_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191026091657&path-prefix=es'
    },
    {
      name: 'Boris',
      description: 'Boris tiene la personalidad gruñona, Boris por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Boris invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Boris también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/dc/Boris_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20180914215345&path-prefix=es'
    },
    {
      name: 'Brie',
      description: 'Brie, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/26/Brie_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191026112304&path-prefix=es'
    },
    {
      name: 'Camember',
      description: 'Camember tiene la personalidad gruñona, Camember por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Camember invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Camember también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/1/11/Camember_%28Happy_Home_Designer%29.png/revision/latest?cb=20190114070138&path-prefix=es'
    },
    {
      name: 'Camelio',
      description: 'Camelio, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/79/Camelio_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191117123117&path-prefix=es'
    },
    {
      name: 'Cachemir',
      description: 'Cachemir, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/7a/Cachemir_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042256&path-prefix=es'
    },
    {
      name: 'Cabriola',
      description: 'Cabriola tiene personalidad Normal, por lo que tendrá un carácter amable, cariñoso y tranquilo, Cabriola disfruta de los pasatiempos comunes, tales como la pesca y atrapar bichos, generalmente para mantenerse activa. Cabriola frecuentemente invitará al Jugador a su casa, donde denotara sus preocupaciones por la higiene y la limpieza. Cabriola se sentirá decepcionada u ofendida fácilmente por otros vecinos, en particular con los vecinos perezoso que se despistan fácilmente en sus conversaciones y los vecinos gruñones que tienen una naturaleza grosera e irritable. Ella se llevará bien con otras vecinas normales y vecinas alegres, también se lleve bien con los vecinos esnobs y presumidas, que disfrutan de la buena personalidad afable de Cabriola, que a menudo las complementan.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/2e/Cabriola_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190512005834&path-prefix=es'
    },
    {
      name: 'Bárbara',
      description: 'Bárbara tiene la personalidad alegre, Bárbara parece estar de buen humor a menudo, y es fácil de hacerse amigo de ella. Bárbara tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Bárbara, al igual que otros vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos sus pasatiempos habituales. Las vecinas alegres sueñan con ser famosas en el futuro y leer Miss Nintendique, una revista leída por vecinos presumidas, alegres normales. Bárbara también tendrán un lapso de atención muy corto, lo que significa que pronto olvidará algunos argumentos o tareas que se le dieron al jugador y que no fueron completados, Bárbara puede enfadarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero ella puede molestar y molestarse con los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a la naturaleza de las vecinas alegres, ella puede referirse a los vecinos gruñones como viejos o aburridos que se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/ad/B%C3%A1rbara_%28New_Leaf%29.png/revision/latest/scale-to-width-down/200?cb=20180806072319&path-prefix=es'
    },
    {
      name: 'Brócoli',
      description: 'Brócoli tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Brócoli le encanta la comida y el descanso. Brócoli disfrutará participar en los pasatiempos comunes, generalmente por razones relajantes o para comer, como la pesca. Brócoli se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y buen estado físico, en comparación con el estilo de vida perezoso de Brócoli. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con los vecinos normales, alegre, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumida, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/dc/Br%C3%B3coli_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20200122214648&path-prefix=es'
    },
    {
      name: 'Brito',
      description: 'Brito, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/df/Brito_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20180320012950&path-prefix=es'
    },
    {
      name: 'Burton',
      description: 'Burton tiene la personalidad gruñona, Burton por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Burton invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Burton también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/96/Burton_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20190312232524&path-prefix=es'
    },
    {
      name: 'Brinco',
      description: 'Brinco tiene la personalidad gruñona, Brinco por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Brinco invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Brinco también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/2c/Brinco_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180505135445&path-prefix=es'
    },
    {
      name: 'Bronco',
      description: 'Bronco tiene la personalidad gruñona, Bronco por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Bronco invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Bronco también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/42/Bronco_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20190702022552&path-prefix=es'
    },
    {
      name: 'Brisa',
      description: 'Brisa, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/88/Brisa_%28New_Leaf%29.png/revision/latest/scale-to-width-down/200?cb=20180913130305&path-prefix=es'
    },
    {
      name: 'Cabrálex',
      description: 'Cabrálex tiene la personalidad esnob, Cabrálex sera muy educado, amable y caballeroso con el Jugador. Como vecino esnob, Cabrálex acariciará su ego de vez en cuando, hablando de lo genial que es y ocasionalmente coqueteará con las jugadoras femeninas, como todo vecino esnob, Cabrálex tiene una obsesión con su imagen y presumirá de su blog personal o la ropa que lleve puesta. En general, Cabrálex se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con los vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c3/Cabr%C3%A1lex_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180822051851&path-prefix=es'
    },
    {
      name: 'Bunga',
      description: 'Bunga tiene la personalidad gruñona, Bunga por lo general aparece de mal humor y agitado y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Bunga invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alregres, ya que parecerán sobre estimuladas o inmaduras. Bunga también disfrutará esparciendo rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/cb/Bunga_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180727184013&path-prefix=es'
    },
    {
      name: 'Bruno',
      description: 'Bruno, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/1/1c/Bruno_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20181207020421&path-prefix=es'
    },
    {
      name: 'Cana',
      description: 'Cana tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Cana le encanta la comida y el descanso. Cana disfrutará de participar en los pasatiempos comunes, generalmente por razones relajantes o para comer, como la pesca. Cana se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y buen estado físico, en comparación con el estilo de vida perezoso de Cana. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con los vecinos normales, alegre, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumida, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/e8/Cana_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180108033104&path-prefix=es'
    },
    {
      name: 'Camila',
      description: 'Camila tiene la personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Como vecina presumida, Camila primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También habla sobre el estilo y la apariencia de otros vecinos, por lo general vecinas normales, alegres y otras vecinas presumidas. Pronto se pondrá en contacto con el Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Camila no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos. porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/8b/Camila_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/150?cb=20180911004600&path-prefix=es'
    },
    {
      name: 'Canberra',
      description: 'Canberra tiene la personalidad dulce, cuando se haga amiga del Jugador, le dará consejos para pelear y le enseñará formas de relajarse. Canberra también le dará Medicina al jugador si ha sido picado por abejas. Al igual que todos las vecinas dulces, Canberra tendrá un carácter rudo, pero aún se preocupará mucho por su apariencia. Canberra tiende a quedarse despierta hasta muy tarde. Es fácil hacerse una idea de ella, ya que desarrollar amistades con las vecinas dulces es simple debido a su naturaleza afectuosa y protectora. Canberra tratará al jugador con respeto e incluso se ofrecerá a luchar contra cualquiera que le cause problemas. Canberra puede llevarse bien con los vecinos atléticos ya que comparten su amor por los deportes, los vecinos perezosos por lo tranquilos que son y las vecinas alegres. Sin embargo, ella puede entrar en conflicto con las vecinas presumidas y gruñones, que difunden rumores sobre los demás, y ella puede tener problemas para llevarse bien con las vecinas normales.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6d/Canberra_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190702023109&path-prefix=es'
    },
    {
      name: 'Carla',
      description: 'Carla tratará al jugador con respeto e incluso se ofrecerá a luchar contra cualquiera que le cause problemas al jugador. Carla puede llevarse bien con los vecinos atléticos ya que comparten su amor por los deportes, los vecinos perezosos por lo tranquilos que son y las vecinas alegre. Sin embargo, ella puede entrar en conflicto con los vecinos presumidos y gruñón, que difunden rumores sobre los demás, y ella puede tener problemas para llevarse bien con las vecinas normales.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/99/Carla_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/150?cb=20180822064602&path-prefix=es'
    },
    {
      name: 'Carmelo',
      description: 'Carmelo tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Carmelo le encanta la comida y el descanso. Carmelo disfrutará participar en los pasatiempos comunes, generalmente por razones relajantes o para comer, como la pesca. Carmelo se llevará bien con los otros vecinos, pero pueden ofender o confundir a los vecinos atlético, que tienen un estilo de vida de ejercicio y buen estado físico, en comparación con el estilo de vida perezoso de Carmelo. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con los vecinos normales, alegres, esnob, dulces y otros vecinos perezosos, y ocasionalmente vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumida, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/87/Carmelo_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042258&path-prefix=es'
    },
    {
      name: 'Carmen',
      description: 'Carmen, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/65/Carmen.png/revision/latest/scale-to-width-down/120?cb=20191109114409&path-prefix=es'
    },
    {
      name: 'Arsenio',
      description: 'A partir de New Leaf, Arsenio tiene la personalidad esnob, será caballeroso, amable y educado con el jugador. Al igual que otros vecinos de su misma personalidad, acariciará su ego de vez en cuando, hablando de lo genial que es. Se llevará bien con el resto de vecinos, aunque pueda entrar en conflicto con algún vecino de personalidad gruñona. Es muy optimista y ocasionalmente coqueteará con una jugadora mujer. Tiene una obsesión con su imagen, y siempre presumirá de la ropa que lleva. En sus conversaciones, hablará de moda entre otros temas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/cf/Arsenio_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20181224053747&path-prefix=es'
    },
    {
      name: 'Cerillo',
      description: 'Cerillo tiene la personalidad gruñona, Cerillo por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Cerillo invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Cerillo también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/5b/Cerillo_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190620074215&path-prefix=es'
    },
    {
      name: 'Carrot',
      description: 'Carrot, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/d9/Carrot_%28D%C5%8Dbutsu_no_Mori_e_%29.png/revision/latest?cb=20181006054805&path-prefix=es'
    },
    {
      name: 'Celeste',
      description: 'Celeste tiene la personalidad alegre, por lo que a menudo estará de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Celeste tendrá la tendencia a reaccionar de forma exagerada en conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Celeste, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos más comunes, sueña con ser famosa en el futuro y disfrutara leer la revista Miss Nintendique. Celeste también tendrá un lapso de atención muy corto, lo que significa que olvidará algunas conversaciones o encargos que le dio al jugador que no fueron completadas muy rápido, Celeste puede enojarse fácilmente en una conversación cuando no se esta de acuerdo con ella. Celeste se llevará bien con los otros vecinos, particularmente con los perezoso, normal, atlético, dulce, y otras vecinas alegres, pero ella se puede molestar y ser molestada por los vecinos gruñón y esnobs, cuyas personalidades difieren. Debido a la naturaleza de Celeste, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/d8/Celeste_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190611201853&path-prefix=es'
    },
    {
      name: 'Cervasio',
      description: 'Cervasio tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Cervasio le encanta la comida y el descanso. Cervasio disfrutará de participar en los pasatiempos comunes, generalmente por razones que involucran relajarse o comer, como la pesca. Cervasio se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y de buen estado físico, en comparación con el estilo de vida perezoso de Cervasio. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con vecinos normales, alegres, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/4c/Cervasio_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180603023637&path-prefix=es'
    },
    {
      name: 'Chai',
      description: 'Chai, al ser de personalidad alegre, normalmente estará de buen humor, y será fácil de hacerse amigo de ella. Reaccionará de forma exagerada en las conversaciones y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa. Las vecinas alegres suelen leer Miss Nintendique, una revista que se centra en la moda y la belleza. Se llevará con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero se puede molestar con los vecinos gruñones y esnobs, cuyas personalidades difieren con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c6/Chai_%28New_Leaf%29.png/revision/latest/scale-to-width-down/100?cb=20200325114910&path-prefix=es'
    },
    {
      name: 'Cerecita',
      description: 'Cerecita, al ser de personalidad alegre, normalmente estará de buen humor, y será fácil de hacerse amigo de ella. Reaccionará de forma exagerada en las conversaciones y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa. Las vecinas alegres suelen leer Miss Nintendique, una revista que se centra en la moda y la belleza. Se llevará con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero se puede molestar con los vecinos gruñones y esnobs, cuyas personalidades difieren con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b1/Cerecita_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20200120134710&path-prefix=es'
    },
    {
      name: 'Chelsea',
      description: 'Chelsea, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b5/Chelsea_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20180824054243&path-prefix=es'
    },
    {
      name: 'Champagne',
      description: 'Champagne, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b4/Champagne_DnMe%2B.png/revision/latest?cb=20190418131822&path-prefix=es'
    },
    {
      name: 'Charo',
      description: 'Charo, al ser de personalidad dulce, se comportará de forma cariñosa hacia el jugador y a los vecinos. Se preocupará por la moda, pero no dejará de ser amable y atenta pudiendo regalar al jugador una medicina cuando le pican las abejas. Cuando se haga amiga del Jugador, le dará consejos para pelear y le enseñará formas de relajarse. Se llevará bien con vecinos de personalidad atlética, perezosa, esnob y alegre, así como también con otras vecinas de su misma personalidad. Podría entrar con conflicto con vecinos de personalidad gruñona o presumida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/41/Charo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/100?cb=20191110122003&path-prefix=es'
    },
    {
      name: 'Chico',
      description: 'Chico, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/ee/Chico.png/revision/latest/scale-to-width-down/120?cb=20191109105429&path-prefix=es'
    },
    {
      name: 'Chusquis',
      description: 'Chusquis tiene la personalidad Esnob, Chusquis sera muy educado, amable y caballeroso con el jugador, su personalidad parece ser una mezcla de los otros tipos de personalidad, Chusquis acariciará su ego de vez en cuando, hablando de lo genial que es y ocasionalmente coqueteará con las jugadoras femeninas, como todo vecino esnob, Chusquis tiene una obsesión con su imagen y presumirá de su blog personal o la ropa que lleve puesta. Chusquis se lleva bien con la mayoría de los vecinos, En general, Chusquis se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con los vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b1/Chusquis_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20181224054927&path-prefix=es'
    },
    {
      name: 'Chuchi',
      description: 'Chuchi, al ser de personalidad alegre, normalmente estará de buen humor, y será fácil de hacerse amigo de ella. Reaccionará de forma exagerada en las conversaciones y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa. Las vecinas alegres suelen leer Miss Nintendique, una revista que se centra en la moda y la belleza. Se llevará con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero se puede molestar con los vecinos gruñones y esnobs, cuyas personalidades difieren con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/04/Chuchi_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191109111156&path-prefix=es'
    },
    {
      name: 'Chocolat',
      description: 'Chocolat tiene la personalidad alegre, por lo que a menudo parece estar de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Chocolat tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Chocolat, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos habituales. Chocolat sueña con convertirse en famosa en el futuro y leer la Miss Nintendique, una revista invisible leída por vecinas presumida, alegre, normales, y otros vecinos. Chocolat también tendrá un lapso de atención muy corto, lo que significa que olvidará algunos argumentos o tareas que se le dieron al jugador que no fueron completadas muy rápido, Chocolat puede enojarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezoso, normales, atléticos, dulces, y otros vecinos alegre, pero ella se puede molestar y molestar a los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a su naturaleza de Chocolat, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/1/1d/Chocolat_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042302&path-prefix=es'
    },
    {
      name: 'Chema',
      description: 'Chema, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/8e/Chema_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191205211701&path-prefix=es'
    },
    {
      name: 'Chipi',
      description: 'Chipi, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/d8/Chipi_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20190702023419&path-prefix=es'
    },
    {
      name: 'Cirano',
      description: 'Cirano tiene la personalidad gruñona, Cirano por lo general parecerá estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Cirano tiene una edad avanzada e invertirá su tiempo en su hobby, con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidos y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con las vecinas alegres, debido a su actitud sobre estimulada e inmadura. Cirano también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/a6/Cirano_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/150?cb=20190810011839&path-prefix=es'
    },
    {
      name: 'Cleo',
      description: 'Cleo tiene la personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Cleo primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También hablará sobre el estilo y la apariencia de otros vecinos, por lo general de vecinas normales, alegres y otras vecinas presumidas. Pronto se apegara hacia Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Cleo no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos, porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras y similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/de/Cleo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20191126141656&path-prefix=es'
    },
    {
      name: 'Clorinda',
      description: 'Clorinda, al ser de personalidad alegre, normalmente estará de buen humor, y será fácil de hacerse amigo de ella. Reaccionará de forma exagerada en las conversaciones y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa. Las vecinas alegres suelen leer Miss Nintendique, una revista que se centra en la moda y la belleza. Se llevará con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero se puede molestar con los vecinos gruñones y esnobs, cuyas personalidades difieren con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/3e/Clorinda_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191226165509&path-prefix=es'
    },
    {
      name: 'Clara',
      description: 'Clara, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6a/Clara_DnMe%2B.png/revision/latest/scale-to-width-down/100?cb=20190417102609&path-prefix=es'
    },
    {
      name: 'Claudio',
      description: 'Claudio, al ser atlético y deportivo, le gustará mucho hacer ejercicio y suele criticar a los demás sobre su estado físico. Será torpe y despistado con las conversaciones profundas. Se lleva bien con otros atléticos, pero confundido con los perezosos por su estilo de vida. Puede ofender o molestar a los gruñones y presumidos. Aunque en realidad, si le gustan los fósiles porque a veces te puede pedir que le des un fósil cualquiera para entrenar, pero se los pone en su casa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/00/Claudio.png/revision/latest/scale-to-width-down/120?cb=20191126143144&path-prefix=es'
    },
    {
      name: 'Clueca',
      description: 'Clueca, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/01/Clueca.png/revision/latest/scale-to-width-down/100?cb=20120915115856&path-prefix=es'
    },
    {
      name: 'Codrila',
      description: 'Codrila tiene la personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Como vecina presumida, Codrila primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También habla sobre el estilo y la apariencia de otros vecinos, por lo general vecinas normales, alegres y otras vecinas presumidas. Pronto se pondrá en contacto con el Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Codrila no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos. porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/d0/Codrila_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20181105022429&path-prefix=es'
    },
    {
      name: 'Cocoloca',
      description: 'Cocoloca tiene la personalidad normal, por lo que con frecuencia actúa con amabilidad hacia el Jugador. Cocoloca generalmente se lleva bien con los vecinos perezosos, alegres, esnobs, presumidas y otras vecinas normales. Ella puede entrar en conflicto con vecinos gruñones y dulces. Como vecina normal, Cocoloca por lo general se despierta a las 6:00 a.m y es más fácil hacerse amigo de ella que la mayoría. Tanto ella como vecinas alegres suelen mencionar a una amiga imaginaria conocida como Fregonita.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c8/Cocoloca_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042304&path-prefix=es'
    },
    {
      name: 'Colmillo',
      description: 'Colmillo tiene la personalidad gruñona, Colmillo por lo general parecerá estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Colmillo tiene una edad avanzada e invertirá su tiempo en su hobby, con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con las vecinas alegre, debido a su actitud sobre estimulada e inmadura. Colmillo también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/41/Colmillo_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200205015214&path-prefix=es'
    },
    {
      name: 'Comando',
      description: 'Comando tiene la personalidad gruñona, Comando por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Comando invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Comando también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/57/Comando_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20200323032043&path-prefix=es'
    },
    {
      name: 'Corcelia',
      description: 'Corcelia tiene la personalidad dulce, cuando se haga amiga del Jugador, le dará consejos para pelear y le enseñará formas de relajarse. Corcelia también le dará Medicina al jugador si ha sido picado por abejas. Al igual que todos las vecinas dulces, Corcelia tendrá un carácter rudo, pero aún se preocupará mucho por su apariencia. Corcelia tiende a quedarse despierta hasta muy tarde. Es fácil hacerse una idea de ella, ya que desarrollar amistades con las vecinas dulces es simple debido a su naturaleza afectuosa y protectora. Corcelia tratará al jugador con respeto e incluso se ofrecerá a luchar contra cualquiera que le cause problemas. Corcelia puede llevarse bien con los vecinos atléticos ya que comparten su amor por los deportes, los vecinos perezosos por lo tranquilos que son y las vecinas alegres. Sin embargo, ella puede entrar en conflicto con las vecinas presumidas y los vecinos gruñones, que difunden rumores sobre los demás, también puede tener problemas para llevarse bien con las vecinas normales.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/7a/Corcelia_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200313144628&path-prefix=es'
    },
    {
      name: 'Coni',
      description: 'Coni tiene la personalidad alegre, por lo que a menudo estará de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Coni tendrá la tendencia a reaccionar de forma exagerada en conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Coni, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos más comunes, sueña con ser famosa en el futuro y disfrutara leer la revista Miss Nintendique. Coni también tendrá un lapso de atención muy corto, lo que significa que olvidará algunas conversaciones o encargos que le dio al jugador que no fueron completadas muy rápido, Coni puede enojarse fácilmente en una conversación cuando no se esta de acuerdo con ella. Coni se llevará bien con los otros vecinos, particularmente con los perezoso, normal, atlético, dulce, y otras vecinas alegres, pero ella se puede molestar y ser molestada por los vecinos gruñón y esnobs, cuyas personalidades difieren. Debido a la naturaleza de Coni, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/db/Coni_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200221074140&path-prefix=es'
    },
    {
      name: 'Cornelio',
      description: 'Cornelio tiene la personalidad atlética, por lo que tendrá un gran interés en la aptitud física y los deportes. Esto lo hará parecer muy competitivo y desafiante, Cornelio tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, pero a pesar de esto, sera amigable y amable con el Jugador. Como todo vecino atlético, Cornelio está muy interesado en su hobby, y puede competir contra el jugador por atrapar Bichos o Peces, Cornelio se lleva bien con los vecinos esnobs, alegres y dulces, pero tendrá problemas con el estilo de vida relajado de los vecinos perezosos, las vecinos gruñones y presumida también pueden tener problemas para llevarse bien con Cornelio.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c0/Cornelio_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190926075223&path-prefix=es'
    },
    {
      name: 'Cornio',
      description: 'Cornio tiene la personalidad gruñona, Cornio por lo general parecerá estar agitado y de mal humor y es más difícil hacerse amigo de él que los otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Cornio tiene una edad avanzada e invertirá su tiempo en su hobby, con frecuencia desafiará al jugador a varias competiciones. Cornio se entenderá fácilmente con vecinos atléticos, presumidos y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con las vecinas alegres, debido a a su actitud sobre estimulada e inmadura. Cornio también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/08/Cornio_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042309&path-prefix=es'
    },
    {
      name: 'Cousteau',
      description: 'Cousteau, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/a5/Cousteau_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20190809074347&path-prefix=es'
    },
    {
      name: 'Corvilo',
      description: 'Corvilo tiene la personalidad Esnob, Corvilo será muy educado, amable y caballeroso con el jugador. Corvilo acariciará su ego de vez en cuando, hablando de lo genial que es y ocasionalmente coqueteará con las jugadoras femeninas, como todo vecino esnob, Corvilo tiene una obsesión con su imagen y presumirá de su blog personal o la ropa que lleve puesta. En general, Corvilo se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con los vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/51/Corvilo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180603004823&path-prefix=es'
    },
    {
      name: 'Cube',
      description: 'Cube, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/92/Cube_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/150?cb=20180730060900&path-prefix=es'
    },
    {
      name: 'Crispín',
      description: 'Crispín, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/97/Crisp%C3%ADn_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042311&path-prefix=es'
    },
    {
      name: 'Croaldo',
      description: 'Croaldo, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/26/Croaldo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20191122211613&path-prefix=es'
    },
    {
      name: 'Crinaldo',
      description: 'A partir de New Leaf la personalidad de Crinaldo pasa a ser esnob será caballeroso, amable y educado con el jugador. Al igual que otros vecinos de su misma personalidad, acariciará su ego de vez en cuando, hablando de lo genial que es. Se llevará bien con el resto de vecinos, aunque pueda entrar en conflicto con algún vecino de personalidad gruñona. Es muy optimista y ocasionalmente coqueteará con una jugadora mujer. Tiene una obsesión con su imagen, y siempre presumirá de la ropa que lleva. En sus conversaciones, hablará de moda entre otros temas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/a9/Crinaldo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/100?cb=20191116113925&path-prefix=es'
    },
    {
      name: 'Croco',
      description: 'Croco, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/54/Croco_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20181104012412&path-prefix=es'
    },
    {
      name: 'Eloísa',
      description: 'Eloísa tiene una personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Como una vecina presumida, Eloísa primero se mostrará grosera y arrogante con el jugador , a menudo hablando de sí misma y de sus propias experiencias. También habla sobre el estilo y la apariencia de otros vecinas, por lo general vecinas normales, alegres y otras vecinas presumidas. Pronto se pondrá en contacto con el jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Eloísa no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque los vecinos perezosos odian lo que come, y ella no se lleva bien con los vecinos atléticos. porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/0f/Elo%C3%ADsa_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190509072123&path-prefix=es'
    },
    {
      name: 'Eli',
      description: 'Elina tiene la piel de color café. Lleva dos coletas de color castaño y tiene un círculo rojo en la frente. Tiene unas grandes pestañas y unos ojos de color verde. Viste con una camisa roja aloha.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b6/Eli_%28New_Leaf%29.png/revision/latest/scale-to-width-down/200?cb=20190509073824&path-prefix=es'
    },
    {
      name: 'Eli',
      description: 'Eli tiene la personalidad normal, por lo que con frecuencia actúa con amabilidad hacia el Jugador. Eli generalmente se lleva bien con los vecinos perezosos, alegres, esnobs, presumidas y otras vecinas normales. Ella puede entrar en conflicto con vecinos gruñones y dulces. Como vecina normal, Eli por lo general se despierta a las 6:00 a.m y es más fácil hacerse amigo de ella que la mayoría. Tanto ella Como vecinas alegres suelen mencionar a una amiga imaginaria conocida como Fregonita.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b6/Eli_%28New_Leaf%29.png/revision/latest/scale-to-width-down/200?cb=20190509073824&path-prefix=es'
    },
    {
      name: 'Déivid',
      description: 'Déivid, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b8/D%C3%A9ivid_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191116093027&path-prefix=es'
    },
    {
      name: 'Dragonio',
      description: 'Dragonio tiene la personalidad perezosa, por lo que será amigable y fácil de llevar debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Dragonio le encanta la comida y relajarse. Disfrutará participando en los pasatiempos habituales, generalmente por razones relajantes o para comer, como la pesca. Dragonio se llevará bien con otros vecinos, pero pueden ofender o confundir a los vecinos Atléticos, que tienen un estilo de vida conflictivo de ejercicio y estado físico, en comparación con el estilo de vida perezoso de Dragonio. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con los vecinos normales, alegres, esnobs, dulces y otros vecinos perezosos, y ocasionalmente vecinos gruñones, pero pueden de vez en cuando molestar a las vecinos presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/71/Dragonio_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180525233611&path-prefix=es'
    },
    {
      name: 'Draco',
      description: 'Draco piensa de sí mismo que es un Muchacho de Moda o un Romántico, aunque no es cierto. Le gustan los plátanos, los juegos de alimentos y los libros de historietas y de camping. Es muy amable, pero le gusta mucho hablar de sí mismo. Eventualmente puede pedir recados. Si no se le habla durante mucho tiempo puede enfermar (aunque es raro). Suele pedir ropa y leer la Revista Siesta. Su muletilla es polluelo. Le gusta el café mezcla sin leche ni azúcar.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6a/Draco_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191201111522&path-prefix=es'
    },
    {
      name: 'Epona',
      description: 'Epona tiene la personalidad alegre, Epona parece estar de buen humor a menudo, y es fácil de hacerse amigo de ella. Epona tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Epona, al igual que otros vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos sus pasatiempos habituales. Las vecinas alegres sueñan con ser famosas en el futuro y leer Miss Nintendique, una revista leída por vecinos presumidas, alegres normales. Epona también tendrán un lapso de atención muy corto, lo que significa que pronto olvidará algunos argumentos o tareas que se le dieron al jugador y que no fueron completados, Epona puede enfadarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero ella puede molestar y molestarse con los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a la naturaleza de las vecinas alegres, ella puede referirse a los vecinos gruñones como viejos o aburridos que se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6c/Epona_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20190124015239&path-prefix=es'
    },
    {
      name: 'Bayo',
      description: 'Bayo tiene la personalidad perezosa, por lo que será amigable y fácil de llevar debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Bayo le encanta la comida y relajarse. Disfrutará participando en los pasatiempos habituales, generalmente por razones relajantes o para comer, como la pesca. Bayo se llevará bien con otros vecinos, pero pueden ofender o confundir a los vecinos Atléticos, que tienen un estilo de vida conflictivo de ejercicio y estado físico, en comparación con el estilo de vida perezoso de Bayo. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con los vecinos normales, alegres, esnobs, dulces y otros vecinos perezosos, y ocasionalmente vecinos gruñones, pero pueden de vez en cuando molestar a las vecinos presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/47/Bayo_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/100?cb=20190212222124&path-prefix=es'
    },
    {
      name: 'Cándida',
      description: 'Cándida, al ser de personalidad dulce, se comportará muy cariñosa hacia el jugador y a los vecinos. Si al jugador le pican las abejas, le dará medicina. Se preocupará por la moda, pero al mismo tiempo será amable y atenta. Es fácil llevarse con ella. Su gran habilidad es jugar a las cartas. Su cumpleaños es el 3 de mayo. Es muy fiestera y cuando no hay organizadas fiestas siempre piensa en plantear una. La cangurita que lleva dentro (su hija, por decirlo de otra manera) es muy graciosa porque siempre pone los mismos gestos que su madre. Su lema es No por mucho saltar amanece mas temprano.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/3f/C%C3%A1ndida_NL.png/revision/latest/scale-to-width-down/150?cb=20180609170059&path-prefix=es'
    },
    {
      name: 'Cuqui',
      description: 'Cuqui tiene la personalidad Presumida, lo que significa que le encanta el maquillaje y los chismes. Como vecina presumida, Cuqui primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También hablará sobre el estilo y la apariencia de otros Vecinos, por lo general de vecinas normales, alegres y otras vecinas presumidas. Pronto se apegara al Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Cuqui no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras y similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/ce/Cuqui_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200207225052&path-prefix=es'
    },
    {
      name: 'Cuálter',
      description: 'Cuálter, al ser de personalidad esnob, será caballeroso, amable y educado con el jugador. Al igual que otros vecinos de su misma personalidad, acariciará su ego de vez en cuando, hablando de lo genial que es. Se llevará bien con el resto de vecinos, aunque pueda entrar en conflicto con algún vecino de personalidad gruñona. Es muy optimista y ocasionalmente coqueteará con una jugadora mujer. Tiene una obsesión con su imagen, y siempre presumirá de la ropa que lleva. En sus conversaciones, hablará de moda entre otros temas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c7/Cu%C3%A1lter_%28New_Leaf%29.png/revision/latest/scale-to-width-down/100?cb=20191018221137&path-prefix=es'
    },
    {
      name: 'Eto',
      description: 'Eto, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/66/Etoile_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180921040031&path-prefix=es'
    },
    {
      name: 'Eto',
      description: 'Eto, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6b/Eto_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191110120053&path-prefix=es'
    },
    {
      name: 'Cocoloca',
      description: 'Cocoloca tiene la personalidad normal, por lo que con frecuencia actúa con amabilidad hacia el Jugador. Cocoloca generalmente se lleva bien con los vecinos perezosos, alegres, esnobs, presumidas y otras vecinas normales. Ella puede entrar en conflicto con vecinos gruñones y dulces. Como vecina normal, Cocoloca por lo general se despierta a las 6:00 a.m y es más fácil hacerse amigo de ella que la mayoría. Tanto ella Como vecinas alegres suelen mencionar a una amiga imaginaria conocida como Fregonita.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c8/Cocoloca_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042304&path-prefix=es'
    },
    {
      name: 'Estallón',
      description: 'Estallón, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/8c/Estall%C3%B3n_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20181029020026&path-prefix=es'
    },
    {
      name: 'Felyne',
      description: 'Felyne tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Felyne le encanta la comida y el descanso. Felyne disfrutará de participar en los pasatiempos comunes, generalmente por razones que involucran relajarse o comer, como la pesca. Felyne se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y de buen estado físico, en comparación con el estilo de vida perezoso de Felyne. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con vecinos normales, alegres, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/72/Felyne_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/100?cb=20200126235721&path-prefix=es'
    },
    {
      name: 'Fruity',
      description: 'Fruity es un pato negro de cabello rubio y ojos grandes. El exterior de sus alas es blanco. Sus patas son amarillas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b3/Fruity_DnMe%2B.png/revision/latest/scale-to-width-down/100?cb=20190418103450&path-prefix=es'
    },
    {
      name: 'Ganon',
      description: 'Ganon tiene la personalidad atlética, y tiene un gran interés en los deportes y el culturismo. Ganon es hiperactivo y motivado, pero a menudo se muestra egoísta y denso. Como un vecino atlético, Ganon está muy interesado en su hobby, y puede competir contra el jugador por atrapar Bichos o Peces. Ganon se lleva bien con los vecinos alegres, gruñones y dulces, pero entran en conflicto con el estilo de vida relajado de los vecinos perezosos. Las vecinas presumidas también pueden ser difíciles de llevarse bien con Ganon.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6b/Ganon_%28New_Leaf%29.png/revision/latest/scale-to-width-down/100?cb=20180922053646&path-prefix=es'
    },
    {
      name: 'Dentina',
      description: 'Dentina tiene la personalidad alegre, por lo que a menudo parece estar de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Dentina tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Dentina, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos habituales. Dentina sueña con convertirse en famosa en el futuro y leer la Miss Nintendique, una revista invisible leída por vecinas presumida, alegre, normales, y otros vecinos. Dentina también tendrá un lapso de atención muy corto, lo que significa que olvidará algunos argumentos o tareas que se le dieron al jugador que no fueron completadas muy rápido, Dentina puede enojarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezoso, normales, atléticos, dulces, y otros vecinos alegre, pero ella se puede molestar y molestar a los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a su naturaleza de Dentina, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/2c/Dentina_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20181213051153&path-prefix=es'
    },
    {
      name: 'Deira',
      description: 'Deira, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/64/Deira_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/150?cb=20180315032841&path-prefix=es'
    },
    {
      name: 'César',
      description: 'César tiene la personalidad gruñona, César por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, César invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. César también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/56/C%C3%A9sar_%28New_Horizons%29.png/revision/latest/scale-to-width-down/200?cb=20200221074139&path-prefix=es'
    },
    {
      name: 'Cándido',
      description: 'Cándido, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/65/C%C3%A1ndido_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191101134439&path-prefix=es'
    },
    {
      name: 'Cuzco',
      description: 'Cuzco tiene la personalidad gruñona, por lo general aparece de mal humor y agitado y son más difíciles de hacerse amigos que otros tipos de vecinos. Cuando habla con el jugador, Cuzco tienden a enojarse si el jugador no está de acuerdo con él o se niega a hacer un favor. Al igual que todos los vecinos gruñones, Cuzco invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con Atléticos atontados, presumidas y otros gruñones, y en ocasiones también vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas Alegres, ya que parecerán sobre estimuladas o inmaduras.Cuzco también disfrutará esparciendo rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/29/Cuzco_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/100?cb=20180722061426&path-prefix=es'
    },
    {
      name: 'Elvis',
      description: 'Elvis tiene la personalidad gruñona, Elvis por lo general parecerá estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Elvis tiene una edad avanzada e invertirá su tiempo en su hobby, con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con las vecinas alegres, debido a a su actitud sobre estimulada e inmadura. Elvis también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/a0/Elvis_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190313015139&path-prefix=es'
    },
    {
      name: 'Encina',
      description: 'Encina tiene personalidad Normal, por lo que tendrá un carácter amable, cariñoso y tranquilo, Encina disfruta de los pasatiempos comunes, tales como la pesca y atrapar bichos, generalmente para mantenerse activa. Encina frecuentemente invitará al Jugador a su casa, donde denotara sus preocupaciones por la higiene y la limpieza. Encina se sentirá decepcionada u ofendida fácilmente por otros vecinos, en particular con los vecinos perezoso que se despistan fácilmente en sus conversaciones y los vecinos gruñones que tienen una naturaleza grosera e irritable. Ella se llevará bien con otras vecinas normales y vecinas alegres, también se lleve bien con los vecinos esnobs y presumidas, que disfrutan de la buena personalidad afable de Encina, que a menudo las complementan.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/3d/Encina_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200410225553&path-prefix=es'
    },
    {
      name: 'Hipo',
      description: 'Hipo, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/5/56/Hipo.gif/revision/latest?cb=20121202131856&path-prefix=es'
    },
    {
      name: 'Espe',
      description: 'Espe tiene personalidad Normal, por lo que tendrá un carácter amable, cariñoso y tranquilo, Espe disfruta de los pasatiempos comunes, tales como la pesca y atrapar bichos, generalmente para mantenerse activa. Espe frecuentemente invitará al Jugador a su casa, donde denotara sus preocupaciones por la higiene y la limpieza. Espe se sentirá decepcionada u ofendida fácilmente por otros vecinos, en particular con los vecinos perezoso que se despistan fácilmente en sus conversaciones y los vecinos gruñones que tienen una naturaleza grosera e irritable. Ella se llevará bien con otras vecinas normales y vecinas alegres, también se lleve bien con las vecinas presumidas, que disfrutan de la buena personalidad afable de Espe, que a menudo las complementan.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f5/Espe_%28Poblaci%C3%B3n_en_aumento%29.png/revision/latest/scale-to-width-down/150?cb=20180930230558&path-prefix=es'
    },
    {
      name: 'Ernesto',
      description: 'A partir de New Leaf Ernesto tiene la personalidad Esnob, por lo que será caballeroso, amable y educado con el jugador. Al igual que otros vecinos de su misma personalidad, acariciará su ego de vez en cuando, hablando de lo genial que es. Se llevará bien con el resto de vecinos, aunque pueda entrar en conflicto con algún vecino de personalidad gruñona. Es muy optimista y ocasionalmente coqueteará con una jugadora mujer. Tiene una obsesión con su imagen, y siempre presumirá de la ropa que lleva. En sus conversaciones, hablará de moda entre otros temas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/86/Ernesto_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042321&path-prefix=es'
    },
    {
      name: 'Erika',
      description: 'Erika tiene la personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Erika primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También hablará sobre el estilo y la apariencia de otros vecinos, por lo general de vecinas normales, alegres y otras vecinas presumidas. Pronto se apegara hacia Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Erika no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos, porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras y similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6e/Erika_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20191015031347&path-prefix=es'
    },
    {
      name: 'Espork',
      description: 'Espork es un vecino perezoso. Puede ofender o confundir a los Vecinos gruñones porque les compara con su abuelo por su personalidad más madura e irritante, por eso no entienden su estilo de vida. Espork suele llevar en poco tiempo diseños que el jugador expone en las Hermanas Manitas. Se interesa mucho por los bichos y te pedirá competiciones sobre cazarlos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/d1/Espork_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191122210751&path-prefix=es'
    },
    {
      name: 'Herminia',
      description: 'Como isleña, tiene una fruta favorita y una fruta a la que es alérgica. Al alimentarla con la fruta para la alergia reducirá la probabilidad de que deje caer bolsas de dinero. Herminia es alérgica a las cerezas, pero ama las manzanas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/34/Herminia_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180725040334&path-prefix=es'
    },
    {
      name: 'Fabiola',
      description: 'Fabiola, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/0a/Fabiola_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20191109145929&path-prefix=es'
    },
    {
      name: 'Eucalín',
      description: 'Eucalín tiene la personalidad esnob, es muy educado, amables y caballeroso. Como vecino esnob, Eucalín se lleva bien con la mayoría de los vecinos, y su personalidad parece ser una mezcla de los otros tipos de personalidades. Como vecino esnob, acariciará su ego de vez en cuando, hablando de lo genial que es. En general, Eucalín se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con los vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/1/15/Eucal%C3%ADn_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/150?cb=20180624041647&path-prefix=es'
    },
    {
      name: 'Eustakio',
      description: 'Eustakio, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/df/Eustakio_%28New_Leaf%29.png/revision/latest/scale-to-width-down/130?cb=20191123170751&path-prefix=es'
    },
    {
      name: 'Hebra',
      description: 'Hebra, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/0b/Hebra_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042658&path-prefix=es'
    },
    {
      name: 'Fardilia',
      description: 'Fardilia, al ser de personalidad alegre, normalmente estará de buen humor, y será fácil de hacerse amigo de ella. Reaccionará de forma exagerada en las conversaciones y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa. Las vecinas alegres suelen leer Miss Nintendique, una revista que se centra en la moda y la belleza. Se llevará con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero se puede molestar con los vecinos gruñones y esnobs, cuyas personalidades difieren con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f2/Fardilia_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191115142859&path-prefix=es'
    },
    {
      name: 'Fauna',
      description: 'Fauna tiene personalidad Normal, por lo que tendrá un carácter amable, cariñoso y tranquilo, Fauna disfruta de los pasatiempos comunes, tales como la pesca y atrapar bichos, generalmente para mantenerse activa. Fauna frecuentemente invitará al Jugador a su casa, donde denotara sus preocupaciones por la higiene y la limpieza. Fauna se sentirá decepcionada u ofendida fácilmente por otros vecinos, en particular con los vecinos perezoso que se despistan fácilmente en sus conversaciones y los vecinos gruñones que tienen una naturaleza grosera e irritable. Ella se llevará bien con otras vecinas normales y vecinas alegres, también se lleve bien con los vecinos esnobs y presumidas, que disfrutan de la buena personalidad afable de Fauna, que a menudo las complementan.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/ed/Fauna_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180306020811&path-prefix=es'
    },
    {
      name: 'Feli',
      description: 'Feli tiene la personalidad normal, por lo que con frecuencia actúa con amabilidad hacia el Jugador. Feli generalmente se lleva bien con los vecinos perezosos, alegres, esnobs, presumidas y otras vecinas normales. Ella puede entrar en conflicto con vecinos gruñones y dulces. Como vecina normal, Feli por lo general se despierta a las 6:00 y es más fácil hacerse amigo de ella que la mayoría. Tanto ella Como vecinas alegres suelen mencionar a una amiga imaginaria conocida como Fregonita.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f9/Felipe_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20191117104128&path-prefix=es'
    },
    {
      name: 'Feli',
      description: 'Feli tiene la personalidad normal, por lo que con frecuencia actúa con amabilidad hacia el Jugador. Feli generalmente se lleva bien con los vecinos perezosos, alegres, esnobs, presumidas y otras vecinas normales. Ella puede entrar en conflicto con vecinos gruñones y dulces. Como vecina normal, Feli por lo general se despierta a las 6:00 y es más fácil hacerse amigo de ella que la mayoría. Tanto ella Como vecinas alegres suelen mencionar a una amiga imaginaria conocida como Fregonita.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/86/Felina_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180917013002&path-prefix=es'
    },
    {
      name: 'Feli',
      description: 'Felina tiene la personalidad alegre, por lo que a menudo parece estar de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Felina tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Felina, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos habituales. Felina sueña con convertirse en famosa en el futuro y leer la Miss Nintendique, una revista invisible leída por vecinas presumida, alegre, normales, y otros vecinos. Felina también tendrá un lapso de atención muy corto, lo que significa que olvidará algunos argumentos o tareas que se le dieron al jugador que no fueron completadas muy rápido, Felina puede enojarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezoso, normales, atléticos, dulces, y otros vecinos alegre, pero ella se puede molestar y molestar a los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a su naturaleza de Felina, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f9/Felipe_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20191117104128&path-prefix=es'
    },
    {
      name: 'Feli',
      description: 'Felina tiene la personalidad alegre, por lo que a menudo parece estar de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Felina tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Felina, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos habituales. Felina sueña con convertirse en famosa en el futuro y leer la Miss Nintendique, una revista invisible leída por vecinas presumida, alegre, normales, y otros vecinos. Felina también tendrá un lapso de atención muy corto, lo que significa que olvidará algunos argumentos o tareas que se le dieron al jugador que no fueron completadas muy rápido, Felina puede enojarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezoso, normales, atléticos, dulces, y otros vecinos alegre, pero ella se puede molestar y molestar a los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a su naturaleza de Felina, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/86/Felina_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180917013002&path-prefix=es'
    },
    {
      name: 'Felina',
      description: 'Felina tiene la personalidad alegre, por lo que a menudo parece estar de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Felina tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Felina, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos habituales. Felina sueña con convertirse en famosa en el futuro y leer la Miss Nintendique, una revista invisible leída por vecinas presumida, alegre, normales, y otros vecinos. Felina también tendrá un lapso de atención muy corto, lo que significa que olvidará algunos argumentos o tareas que se le dieron al jugador que no fueron completadas muy rápido, Felina puede enojarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezoso, normales, atléticos, dulces, y otros vecinos alegre, pero ella se puede molestar y molestar a los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a su naturaleza de Felina, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/86/Felina_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180917013002&path-prefix=es'
    },
    {
      name: 'Feli',
      description: 'Felipe tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Felipe le encanta la comida y el descanso. Felipe disfrutará de participar en los pasatiempos comunes, generalmente por razones que involucran relajarse o comer, como la pesca. Felipe se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y de buen estado físico, en comparación con el estilo de vida perezoso de Felipe. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con vecinos normales, alegres, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f9/Felipe_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20191117104128&path-prefix=es'
    },
    {
      name: 'Feli',
      description: 'Felipe tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Felipe le encanta la comida y el descanso. Felipe disfrutará de participar en los pasatiempos comunes, generalmente por razones que involucran relajarse o comer, como la pesca. Felipe se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y de buen estado físico, en comparación con el estilo de vida perezoso de Felipe. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con vecinos normales, alegres, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/8/86/Felina_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180917013002&path-prefix=es'
    },
    {
      name: 'Felipe',
      description: 'Felipe tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Felipe le encanta la comida y el descanso. Felipe disfrutará de participar en los pasatiempos comunes, generalmente por razones que involucran relajarse o comer, como la pesca. Felipe se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y de buen estado físico, en comparación con el estilo de vida perezoso de Felipe. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con vecinos normales, alegres, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/f/f9/Felipe_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20191117104128&path-prefix=es'
    },
    {
      name: 'Hanalulú',
      description: 'Hanalulú, al ser de personalidad normal, será tímida y cariñosa con el jugador y los vecinos. Al igual que otras vecinas de la misma personalidad, será obsesiva con la limpieza e higiene. Se llevará bien con vecinos de personalidad esnob, perezosa e incluso también con vecinos de su misma personalidad. Puede llevarse mal con vecinos de personalidad gruñona debido a su naturaleza grosera y arrogante. Por lo general, hablará de otros vecinos en lugar de hablar sobre ella misma y sus problemas. También parece disfrutar de la cocina y con frecuencia le preguntará al jugador sobre comida.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b3/Hanalul%C3%BA_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180520051933&path-prefix=es'
    },
    {
      name: 'Franela',
      description: 'Franela tiene la personalidad alegre, por lo que a menudo estará de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Franela tendrá la tendencia a reaccionar de forma exagerada en conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Franela, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos más comunes, sueña con ser famosa en el futuro y disfrutara leer la revista Miss Nintendique. Franela también tendrá un lapso de atención muy corto, lo que significa que olvidará algunas conversaciones o encargos que le dio al jugador que no fueron completadas muy rápido, Franela puede enojarse fácilmente en una conversación cuando no se esta de acuerdo con ella. Franela se llevará bien con los otros vecinos, particularmente con los perezoso, normal, atlético, dulce, y otras vecinas alegres, pero ella se puede molestar y ser molestada por los vecinos gruñón y esnobs, cuyas personalidades difieren. Debido a la naturaleza de Franela, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/79/Franela_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042653&path-prefix=es'
    },
    {
      name: 'Fleco',
      description: 'Fleco tiene la personalidad esnob, Fleco sera muy educado, amable y caballeroso con el Jugador. Como vecino esnob, Fleco acariciará su ego de vez en cuando, hablando de lo genial que es y ocasionalmente coqueteará con las jugadoras femeninas, como todo vecino esnob, Fleco tiene una obsesión con su imagen y presumirá de su blog personal o la ropa que lleve puesta. En general, Fleco se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con los vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/30/Fleco_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20190721040549&path-prefix=es'
    },
    {
      name: 'Flopi',
      description: 'Flopi, al ser de personalidad alegre, normalmente estará de buen humor, y será fácil de hacerse amigo de ella. Reaccionará de forma exagerada en las conversaciones y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa. Las vecinas alegres suelen leer Miss Nintendique, una revista que se centra en la moda y la belleza. Se llevará con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero se puede molestar con los vecinos gruñones y esnobs, cuyas personalidades difieren con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/2e/Flopi_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191115145532&path-prefix=es'
    },
    {
      name: 'Gloria',
      description: 'Gloria tratará al jugador con respeto e incluso se ofrecerá a luchar contra cualquiera que le cause problemas al jugador. Gloria puede llevarse bien con los vecinos atléticos ya que comparten su amor por los deportes, los vecinos perezosos por lo tranquilos que son y las vecinas alegre. Sin embargo, ella puede entrar en conflicto con los vecinos presumidos y gruñón, que difunden rumores sobre los demás, y ella puede tener problemas para llevarse bien con las vecinas normales.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/e9/Gloria_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20180727192058&path-prefix=es'
    },
    {
      name: 'Gen',
      description: 'Gen, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/7f/Gen_DnMe%2B.png/revision/latest/scale-to-width-down/100?cb=20190417090657&path-prefix=es'
    },
    {
      name: 'Germán',
      description: 'Como isleño, Germán tiene una fruta favorita y una fruta a la que es alérgico. Alimentarlo con la fruta a la que es alérgico reducirá la posibilidad de que deje caer bolsas de dinero. Germán es alérgico a las cerezas y ama los melocotones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/e4/Germ%C3%A1n_NH.png/revision/latest/scale-to-width-down/120?cb=20200411154048&path-prefix=es'
    },
    {
      name: 'Gorrelmo',
      description: 'En general, Gorrelmo se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/a8/Gorrelmo_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/100?cb=20180829234649&path-prefix=es'
    },
    {
      name: 'Bambina',
      description: 'Bambina tiene la personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Como vecina presumida, Bambina primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También habla sobre el estilo y la apariencia de otros vecinos, por lo general vecinas normales, alegres y otras vecinas presumidas. Pronto se pondrá en contacto con el Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Bambina no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos. porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c9/Bambina_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180823042348&path-prefix=es'
    },
    {
      name: 'Filberto',
      description: 'Filberto, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/4a/Filberto_%28Wellcome_amiibo%29.png/revision/latest/scale-to-width-down/200?cb=20180618040011&path-prefix=es'
    },
    {
      name: 'Franfuá',
      description: 'Franfuá, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/61/Franfu%C3%A1_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190519050006&path-prefix=es'
    },
    {
      name: 'Fredo',
      description: 'Fredo tiene la personalidad perezosa, por lo que será amigable y fácil de llevar debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Fredo le encanta la comida y relajarse. Disfrutará participando en los pasatiempos habituales, generalmente por razones relajantes o para comer, como la pesca. Fredo se llevará bien con otros vecinos, pero pueden ofender o confundir a los vecinos Atléticos, que tienen un estilo de vida conflictivo de ejercicio y estado físico, en comparación con el estilo de vida perezoso de Fredo. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con los vecinos normales, alegres, esnobs, dulces y otros vecinos perezosos, y ocasionalmente vecinos gruñones, pero pueden de vez en cuando molestar a las vecinos presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/92/Fredo_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190421211709&path-prefix=es'
    },
    {
      name: 'Frida',
      description: 'Frida, al ser presumida, puede mostrar desprecio frente otros vecinos, especialmente vecinos perezosos y atléticos. Es egocéntrica, de mal genio y sarcástica, pero mientrás el Jugador hable cada vez más con ella, más buena se volverá. Al principio, ella es alegre, pero cuanto más amigos tenga, se le verá más presumida. Le encanta los muebles blancos, y a veces se los pide al jugador, por lo que le gustarán los muebles regios. Su muletilla es heladito.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/1/1e/Frida_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200207231511&path-prefix=es'
    },
    {
      name: 'Furio',
      description: 'Furio, al ser de personalidad esnob, será caballeroso, amable y educado con el jugador. Al igual que otros vecinos de su misma personalidad, acariciará su ego de vez en cuando, hablando de lo genial que es. Se llevará bien con el resto de vecinos, aunque pueda entrar en conflicto con algún vecino de personalidad gruñona. Es muy optimista y ocasionalmente coqueteará con una jugadora mujer. Tiene una obsesión con su imagen, y siempre presumirá de la ropa que lleva. En sus conversaciones, hablará de moda entre otros temas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/a2/Furio_%28Happy_Home_Designer%29.png/revision/latest?cb=20190625022807&path-prefix=es'
    },
    {
      name: 'Holden',
      description: 'Holden es un hámster de color amarillo con las puntas de las patas de color blanco, tiene ojos como los de Fueki y lleva una gorra roja en la cabeza. Extrañamente, parece tener pestañas que sobresalen de sus iris en lugar de sus párpados, esto solo puede ser pensado para ser parte del diseño de sus iris. Su diseño, y sus respectivos muebles y prendas de vestir, se basan en la empresa japonesa conocida como cosméticos Fueki.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c3/Holden_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20181224060057&path-prefix=es'
    },
    {
      name: 'Harpo',
      description: 'Como cualquier vecino gruñón, Harpo parecerá maduro en comparación con otros vecinos de otra personalidad. Esto hará que parezca menos preciado e insensible hacia otros vecinos, entre ellos el jugador. A pesar de esto, él aprenderá a confiar en el jugador, y lo considerará su único amigo. Se podrán disfrutar de los hobbies habituales, pero no suele participar en competiciones. Se va a encontrar dificultades para relacionarse vecinos normales y alegres, a los que se critican al hablar con otros vecinos. Él se lleva bien con los aldeanos perezosos y presumidos, así como otros vecinos gruñones, con los que comparten profundas conversaciones. Él, en ocasiones, molesta a algún vecino normal.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/42/Harpo.png/revision/latest/scale-to-width-down/150?cb=20130717165956&path-prefix=es'
    },
    {
      name: 'Hans',
      description: 'Hans tiene la personalidad esnob, Hans sera muy educado, amable y caballeroso con el Jugador. Como vecino esnob, Hans acariciará su ego de vez en cuando, hablando de lo genial que es y ocasionalmente coqueteará con las jugadoras femeninas, como todo vecino esnob, Hans tiene una obsesión con su imagen y presumirá de su blog personal o la ropa que lleve puesta. En general, Hans se lleva bien con los vecinos perezosos, normales, presumidas y alegres, y puede entrar en conflicto con los vecinos gruñones.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/22/Hans_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20180617200343&path-prefix=es'
    },
    {
      name: 'Güiñón',
      description: 'Güiñón tiene la personalidad gruñona, Güiñón por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Güiñón invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Güiñón también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/a/a2/G%C3%BCi%C3%B1%C3%B3n_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190424020955&path-prefix=es'
    },
    {
      name: 'Jane',
      description: 'Jane tiene la personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Como vecina presumida, Jane primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También habla sobre el estilo y la apariencia de otros vecinos, por lo general vecinas normales, alegres y otras vecinas presumidas. Pronto se pondrá en contacto con el Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Jane no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos. porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/ea/Jane_%28Poblaci%C3%B3n_en_aumento%29.png/revision/latest/scale-to-width-down/150?cb=20180728023847&path-prefix=es'
    },
    {
      name: 'Jaime',
      description: 'Jaime, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/42/Jaime_%28New_Leaf%29.png/revision/latest/scale-to-width-down/100?cb=20191116142516&path-prefix=es'
    },
    {
      name: 'Hipóleo',
      description: 'Hipóleo, al ser de personalidad esnob, será caballeroso, amable y educado con el jugador. Al igual que otros vecinos de su misma personalidad, acariciará su ego de vez en cuando, hablando de lo genial que es. Se llevará bien con el resto de vecinos, aunque pueda entrar en conflicto con algún vecino de personalidad gruñona. Es muy optimista y ocasionalmente coqueteará con una jugadora mujer. Tiene una obsesión con su imagen, y siempre presumirá de la ropa que lleva. En sus conversaciones, hablará de moda entre otros temas.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/0/0f/Hip%C3%B3leo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191116125227&path-prefix=es'
    },
    {
      name: 'Gorbaché',
      description: 'Gorbaché tiene la personalidad gruñona, Gorbaché por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Gorbaché invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Gorbaché también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/3c/Gorbach%C3%A9_%28New_Horizons%29.png/revision/latest/scale-to-width-down/120?cb=20200221074156&path-prefix=es'
    },
    {
      name: 'Gastón',
      description: 'Gastón tiene la personalidad gruñona, Gastón por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Gastón invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Gastón también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/67/Gast%C3%B3n_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042654&path-prefix=es'
    },
    {
      name: 'Gatomán',
      description: 'Gatomán, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/d/d0/Gatom%C3%A1n_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/100?cb=20180525232938&path-prefix=es'
    },
    {
      name: 'Groucho',
      description: 'Groucho tiene la personalidad gruñona, Groucho por lo general aparece estar agitado y de mal humor y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el Jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Groucho invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Groucho también disfrutará esparcir rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/4f/Groucho_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20190225103027&path-prefix=es'
    },
    {
      name: 'Gladis',
      description: 'Gladis tiene personalidad Normal, por lo que tendrá un carácter amable, cariñoso y tranquilo, Gladis disfruta de los pasatiempos comunes, tales como la pesca y atrapar bichos, generalmente para mantenerse activa. Gladis frecuentemente invitará al Jugador a su casa, donde denotara sus preocupaciones por la higiene y la limpieza. Gladis se sentirá decepcionada u ofendida fácilmente por otros vecinos, en particular con los vecinos perezoso que se despistan fácilmente en sus conversaciones y los vecinos gruñones que tienen una naturaleza grosera e irritable. Ella se llevará bien con otras vecinas normales y vecinas alegres, también se lleve bien con los vecinos esnobs y presumidas, que disfrutan de la buena personalidad afable de Gladis, que a menudo las complementan.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/2f/Gladis_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200221074148&path-prefix=es'
    },
    {
      name: 'Gus',
      description: 'Gus tiene la personalidad atlética, por lo que tendrá un gran interés en la aptitud física y los deportes. Esto lo hará parecer muy competitivo y desafiante, Gus tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, pero a pesar de esto, sera amigable y amable con el Jugador. Como todo vecino atlético, Gus está muy interesado en su hobby, y puede competir contra el jugador por atrapar Bichos o Peces, Gus se lleva bien con los vecinos esnobs, alegres y dulces, pero tendrá problemas con el estilo de vida relajado de los vecinos perezosos, las vecinos gruñones y presumida también pueden tener problemas para llevarse bien con Gus.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/24/Gus_%28New_Horizons%29.png/revision/latest/scale-to-width-down/200?cb=20200221074156&path-prefix=es'
    },
    {
      name: 'Jubei',
      description: 'Jubei, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/ef/Jubei_%28D%C5%8Dbutsu_no_Mori_e_%29.png/revision/latest/scale-to-width-down/100?cb=20180613000815&path-prefix=es'
    },
    {
      name: 'Jones',
      description: 'Jones, al ser de personalidad atlética, le gustará mucho hacer ejercicio y criticar a los demás por su estado físico. Esto lo hará parecer competitivo y desafiante, tendrá una actitud hiperactiva y motivada, pero a menudo actuará de forma egoísta y pesada, a pesar de esto, es amigable con el jugador. Normalmente, en sus conversaciones hablará sobre el deporte y sus músculos. Será torpe y despistado con las conversaciones profundas. Se llevará bien con otros vecinos atléticos, pero podría sentirse confundido con los vecinos perezosos por su estilo de vida relajado. Puede ofender o molestar a los vecinos de personalidad gruñona.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/4/4e/Jones_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190313030043&path-prefix=es'
    },
    {
      name: 'Joe',
      description: 'Joe, al ser de personalidad gruñona, al principio será algo grosero con el jugador. Será algo difícil ganarse su amistad. A medida que el jugador se haga amigo de él, se volverá más amable. En sus conversaciones, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Se entenderá bien con vecinos atléticos, vecinas presumidas y con otros vecinos gruñones, en ocasiones también con vecinas normales. Admirará el estilo de vida relajado de los vecinos de personalidad perezosa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/2d/Joe_DnMe%2B.png/revision/latest/scale-to-width-down/100?cb=20190419104851&path-prefix=es'
    },
    {
      name: 'Jimena',
      description: 'Jimena, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/29/Jimena_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191217181401&path-prefix=es'
    },
    {
      name: 'Jarrea',
      description: 'Jarrea, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/2/20/Jarrea_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191130124710&path-prefix=es'
    },
    {
      name: 'Jacobo',
      description: 'Jacobo, al ser de personalidad perezosa, será amigable debido a su estilo de vida. Como a todos los vecinos perezosos, le gustará la comida, relajarse y pescar. Se llevará bien con otros vecinos, pero podría ofender a los vecinos atléticos, que tienen un estilo de vida de ejercicio y condición física, en comparación con el estilo de vida perezoso. Se llevará bien con otros vecinos perezosos con quienes hablará sobre comida, cómics o superhéroes.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/e/e2/Jacobo_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191223130751&path-prefix=es'
    },
    {
      name: 'Jacinto',
      description: 'Jacinto, al ser perezoso, no dejará de pensar en comida y no entenderá la forma de vida de los vecinos atléticos y presumidos, porque no entiende sus estilos de vida, ya que él es relajante y tiene un gran amor a los alimentos. A las vecinas presumidas le suele decir sus planes de cena, les da ideas, y soltará sugerencias de que vendrá a la cena. Tiene una personalidad muy abierta. Con el tiempo, Jacinto irá cambiando y se aficionará a la pesca en vez de quedarse casi siempre en casa.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/7/71/Jacinto_%28New_Leaf%29.png/revision/latest/scale-to-width-down/100?cb=20191116135400&path-prefix=es'
    },
    {
      name: 'Katia',
      description: 'Katia tiene la personalidad alegre, por lo que a menudo parece estar de buen humor, y es fácil de hacerse amigo de ella. Como vecina alegre, Katia tendrá la tendencia a reaccionar de forma exagerada en las conversaciones sobre temas triviales y, por lo general, se emocionará demasiado al ver al Jugador u otros vecinos. Katia, al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa, incluidos los pasatiempos habituales. Katia sueña con convertirse en famosa en el futuro y leer la Miss Nintendique, una revista invisible leída por vecinas presumida, alegre, normales, y otros vecinos. Katia también tendrá un lapso de atención muy corto, lo que significa que olvidará algunos argumentos o tareas que se le dieron al jugador que no fueron completadas muy rápido, Katia puede enojarse fácilmente en una conversación cuando se dicen cosas incorrectas. Ella se lleva bien con otros vecinos, particularmente con los perezoso, normales, atléticos, dulces, y otros vecinos alegre, pero ella se puede molestar y molestar a los vecinos gruñones y esnobs, cuyas personalidades difieren. Debido a su naturaleza de Katia, ella puede mencionar como los vecinos gruñones son viejos o aburridos y se comparan con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b2/Katia_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042702&path-prefix=es'
    },
    {
      name: 'Kasandra',
      description: 'Kasandra, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/cc/Kasandra_%28New_Leaf%29.png/revision/latest/scale-to-width-down/100?cb=20191201124647&path-prefix=es'
    },
    {
      name: 'Kaimán',
      description: 'Kaimán tiene la personalidad perezosa, por lo que será amigable y de trato fácil debido a su estilo de vida relajado. Como a todos los vecinos perezosos, a Kaimán le encanta la comida y el descanso. Kaimán disfrutará de participar en los pasatiempos comunes, generalmente por razones que involucran relajarse o comer, como la pesca. Kaimán se llevará bien con los otros vecinos, pero puede ofender o confundir a los vecinos atléticos, que tienen un estilo de vida de ejercicio y de buen estado físico, en comparación con el estilo de vida perezoso de Kaimán. Se llevará bien con otros vecinos perezosos con los que hablará sobre comida, cómics o superhéroes. También se llevará bien con vecinos normales, alegres, esnobs, dulces, y ocasionalmente con vecinos gruñones, pero pueden de vez en cuando molestar a las vecinas presumidas, que no están de acuerdo con el estilo de vida perezoso.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/9/90/Kaim%C3%A1n_%28New_Horizons%29.png/revision/latest/scale-to-width-down/120?cb=20200320001612&path-prefix=es'
    },
    {
      name: 'Julia',
      description: 'Julia tiene la personalidad presumida, lo que significa que le encanta el maquillaje y los chismes. Como vecina presumida, Julia primero se mostrará grosera y arrogante hacia el Jugador, a menudo hablando de sí misma y de sus propias experiencias. También habla sobre el estilo y la apariencia de otros vecinos, por lo general vecinas normales, alegres y otras vecinas presumidas. Pronto se pondrá en contacto con el Jugador, confinando en ellos sus propios sentimientos pero manteniéndose sutilmente grosera. Julia no se lleva bien con los vecinos perezosos debido a que ellos no se preocupan por su apariencia o porque ellos odian lo que ella come, y ella no se lleva bien con los vecinos atléticos. porque la interrogarán sobre su apariencia física y no se preocupan por la moda. Sin embargo, ella se llevará bien con los vecinos gruñones debido a sus personalidades groseras similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/3/36/Julia_%28New_Horizons%29.png/revision/latest/scale-to-width-down/150?cb=20200202042701&path-prefix=es'
    },
    {
      name: 'Isadora',
      description: 'Isadora, al ser de personalidad presumida, al principio será grosera y arrogante hacia el jugador, a menudo hablando de sí misma y de sus propias experiencias. También criticará la apariencia del resto de los vecinos. Con el paso del tiempo se encariñará con el jugador. Le será difícil socializar con la mayoría de vecinos, cuyos estilos de vida difieren. No entenderá a los vecinos de personalidad perezosa por su estilo de vida relajado, así como tampoco entenderá a los vecinos de personalidad atlética por su estilo de vida deportivo y su rechazo hacia la moda. Sin embargo, se llevará bien con los vecinos de personalidad gruñona debido a sus personalidades similares.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/6/6e/Isadora_%28New_Leaf%29.png/revision/latest/scale-to-width-down/150?cb=20191116110650&path-prefix=es'
    },
    {
      name: 'Hugo',
      description: 'Hugo tiene la personalidad atlética, y tiene un gran interés en los deportes y el culturismo. Hugo es hiperactivo y motivado, pero a menudo se muestra egoísta y denso. Como un vecino atlético, Hugo está muy interesado en su hobby, y puede competir contra el jugador por atrapar Bichos o Peces. Hugo se lleva bien con los vecinos alegres, gruñones y dulces, pero entran en conflicto con el estilo de vida relajado de los vecinos perezosos. Las vecinas presumidas también pueden ser difíciles de llevarse bien con Hugo.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/c/c4/Hugo_%28Happy_Home_Designer%29.png/revision/latest/scale-to-width-down/150?cb=20180722054733&path-prefix=es'
    },
    {
      name: 'Hipólita',
      description: 'Hipólita, al ser de personalidad alegre, normalmente estará de buen humor, y será fácil de hacerse amigo de ella. Reaccionará de forma exagerada en las conversaciones y, por lo general, se emocionará demasiado al ver al jugador u otros vecinos. Al igual que otras vecinas alegres, rara vez se desanimará de hacer cualquier cosa. Las vecinas alegres suelen leer Miss Nintendique, una revista que se centra en la moda y la belleza. Se llevará con otros vecinos, particularmente con los perezosos, normales, atléticos, dulces, y otras vecinas alegres, pero se puede molestar con los vecinos gruñones y esnobs, cuyas personalidades difieren con su personalidad opuesta y optimista.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b7/Hip%C3%B3lita_%28New_Leaf%29.png/revision/latest/scale-to-width-down/120?cb=20191116132904&path-prefix=es'
    },
    {
      name: 'Kabuki',
      description: 'Kabuki tiene la personalidad gruñona, Kabuki por lo general aparece de mal humor y agitado y es más difícil hacerse amigo de él que otros tipos de vecinos. Cuando habla con el jugador, tiende a enojarse si el jugador no está de acuerdo con él o se niega a hacerle un favor. Al igual que todos los vecinos gruñones, Kabuki invertirá en su hobby y con frecuencia desafiará al jugador a varias competiciones. Él se entenderá fácilmente con vecinos atléticos, presumidas y otros vecinos gruñones, y en ocasiones también con vecinos perezosos y normales. Sin embargo, le resultará difícil socializar con vecinas alegres, ya que parecerán sobre estimuladas o inmaduras. Kabuki también disfrutará esparciendo rumores de otros vecinos.',
      img: 'https://vignette.wikia.nocookie.net/animalcrossing/images/b/b6/Kabuki_%28Kappy_Home_Designer%29.png/revision/latest/scale-to-width-down/120?cb=20190403002452&path-prefix=es'
    }
  ]
