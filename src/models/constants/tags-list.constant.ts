import { Tag } from '../interfaces/tag.interface';
import { PLATFORMS } from './platforms.constant';
import { PAGES } from './pages.constant';

export const TAGS_LIST: Array<Tag> =  [
    {
        title: 'home.buttons.quarentene',
        redirectTo: PAGES.RESULTS_END_QUARENTENE.REDIRECTION,
        header: '../../../../assets/images/header-1.jpg'
    },
    {
        title: 'home.buttons.pokemon',
        redirectTo: PAGES.RESULTS_POKEMON.REDIRECTION,
        header: '../../../../assets/images/header-2.jpg'
    },
    {
        title: 'home.buttons.rajoy-quotes',
        redirectTo: PAGES.RESULTS_RAJOY_QUOTES.REDIRECTION,
        header: '../../../../assets/images/header-3.jpg'
    },
    {
        title: 'home.buttons.animal-crossing',
        redirectTo: PAGES.RESULTS_ANIMAL_CROSSING.REDIRECTION,
        header: '../../../../assets/images/header-4.jpg'
    }
];
