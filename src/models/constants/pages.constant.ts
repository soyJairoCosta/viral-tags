export const PAGES = {
    HOME: { REDIRECTION: '', TITLE: 'home.title' },
    GET_DATA: { REDIRECTION: 'get-data', TITLE: 'get-data.title' },
    RESULTS_END_QUARENTENE: { REDIRECTION: 'result-end-quarentene', TITLE: 'result-end-quarentene.title' },
    RESULTS_POKEMON: { REDIRECTION: 'result-pokemon', TITLE: 'result-pokemon.title' },
    RESULTS_RAJOY_QUOTES: { REDIRECTION: 'result-rajoy-quotes', TITLE: 'result-rajoy-quotes.title' },
    RESULTS_ANIMAL_CROSSING: { REDIRECTION: 'result-animal-crossing', TITLE: 'result-animal-crossing.title' },
}