export interface Tag {
    platform?: string;
    title: string;
    header: string;
    redirectTo: string;
}