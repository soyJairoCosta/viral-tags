export interface PoshHippieResult {
    poshValue: number;
    modernValue: number;
    choniValue: number;
    hippieValue: number;
}